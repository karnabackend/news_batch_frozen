#!/usr/bin/python
from alert_id_multiple_day_dump import populate_alert
from memsql.common import database
import tornado.options
import tornado.ioloop
import tornado.web
import traceback
import datetime
import logging
import pymongo
import pika
import json
import sys
import os

tornado.options.parse_command_line()

#logging.basicConfig( filename='alert_population_server.log', level=logging.INFO, propogate=0 )

# MySQL Connection Details
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

# Mongo DB Connection Details
MONGO_DB_SERVER_URL = os.environ["MONGOURL"]
DB_NAME             = "news"
COLLECTION_NAME     = "data_stream_database"

coll = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# RabbitMQ Connection Details
queue_name         = os.environ["RabbitMQ_News_Init"]
redis_update_queue = queue_name + "_redis_update"

parameters = pika.URLParameters( 'amqp://ankit:root53@146.148.71.201:5672/%2F' )
connection = pika.BlockingConnection(parameters=parameters)

channel1   = connection.channel()
channel1.queue_declare( queue=redis_update_queue, durable=True )

# Password for Security
SERVER_PASSWORD    = "delhisehoonbc"
WILDCARD_PASSWORD  = "breakingbad#"
RED_ALERT_PASSWORD = "mongo_is_slow_as_fuck"
RED_ALERT          = False

class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		logging.info( "MainHandler Hit Time (UTC) : %s"%datetime.datetime.utcnow() )
		logging.info( self.request ) 

		try:
			alert_id = int( self.get_argument( "alert_id", None ) )
			days     = int( self.get_argument( "days"    , None ) )
			password =      self.get_argument( "password", None )
		except:
			logging.error( "%s"%traceback.format_exc() )
			return_response = json.dumps( { "status": 0, "message": "Error in Input." } )
			logging.error( "%s"%traceback.format_exc() )
			logging.info( return_response )
			self.write(   return_response )

		if alert_id == None:
			return_response = json.dumps( { "status": 0, "message": "No Alert ID specified." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if days == None:
			return_response = json.dumps( { "status": 0, "message": "No Days specified." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password == None:
			return_response = json.dumps( { "status": 0, "message": "No Password specified." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password != SERVER_PASSWORD:
			return_response = json.dumps( { "status": 0, "message": "Wrong Password ! Access Denied !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		try:
			conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
			q = 'SELECT * FROM alerts WHERE id = %d AND news = 1 AND alert_status = "active" AND news_status = "active"'%alert_id
			memsql_objects = conn.query( q )
			conn.close()
		except:
			logging.error( "%s"%traceback.format_exc() )
			return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
			logging.error( "%s"%traceback.format_exc() )
			logging.info( return_response )
			self.write(   return_response )

		if memsql_objects == None:
			return_response = json.dumps( { "status": 0, "message": "No Active Alert Found for the Specified Alert ID." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		try:
			# Alert Population and Redis Reset
			populate_alert( alert_id, days )
			json_out = json.dumps( { "alert_id": alert_id } )
			channel1.basic_publish( exchange='', routing_key=redis_update_queue, body=json_out, properties=pika.BasicProperties( delivery_mode = 2, ) )
			return_response = json.dumps( { "status": 1, "message": "Dump Successful !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		except:
			return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
			logging.info( return_response )
			logging.error( "%s"%traceback.format_exc() )
			self.write(   return_response )
			return

class RedAlertHandler(tornado.web.RequestHandler):
	def post(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		logging.info( "RedAlertHandler Hit Time (UTC) : %s"%datetime.datetime.utcnow() )
		logging.info( self.request ) 

		global RED_ALERT

		password           = self.get_argument( "password"          , None )
		red_alert_password = self.get_argument( "red_alert_password", None )
		wildcard_password  = self.get_argument( "wildcard_password" , None )

		if password == SERVER_PASSWORD and red_alert_password == RED_ALERT_PASSWORD and wildcard_password == WILDCARD_PASSWORD:
			RED_ALERT       = True
			return_response = json.dumps( { "status": 1, "message": "Red Alert Now Active ! Prepare to Deploy !" } )
			logging.info( return_response )
			self.write(   return_response )
			return
		else:
			return_response = json.dumps( { "status": 0, "message": "Wrong Password ! Access Denied !" }  )
			logging.info( return_response )
			self.write(   return_response )
			return

class CheckHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		
		logging.info( "CheckHandler Hit Time (UTC) : %s"%datetime.datetime.utcnow() )
		logging.info( self.request ) 

		global RED_ALERT
		
		return_response = json.dumps( { "red alert": RED_ALERT } )
		logging.info( return_response )
		self.write(   return_response )

class DeletionHandler(tornado.web.RequestHandler):
	def post(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		logging.info( "DeletionHandler Hit Time (UTC) : %s"%datetime.datetime.utcnow() )
		logging.info( self.request ) 

		global RED_ALERT

		try:
			alert_id           = int( self.get_argument( "alert_id"          , None ) )
			password           =      self.get_argument( "password"          , None )
			red_alert_password =      self.get_argument( "red_alert_password", None )
			wildcard_password  =      self.get_argument( "wildcard_password" , None )
		except:
			RED_ALERT = False
			logging.error( "%s"%traceback.format_exc() )
			return_response = json.dumps( { "status": 0, "message": "Error in Input. Red Alert Deactivated !" } )
			logging.error( "%s"%traceback.format_exc() )
			logging.info( return_response )
			self.write(   return_response )

		if RED_ALERT == False:
			return_response = json.dumps( { "status": 0, "message": "Red Alert is not Active. Access Denied !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if alert_id == None:
			RED_ALERT = False
			return_response = json.dumps( { "status": 0, "message": "No Alert ID specified. Red Alert Deactivated !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password != SERVER_PASSWORD or red_alert_password != RED_ALERT_PASSWORD or wildcard_password != WILDCARD_PASSWORD:
			RED_ALERT = False
			return_response = json.dumps( { "status": 0, "message": "Wrong Password Combination ! Red Alert Deactivated !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		try:
			conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
			q = 'SELECT * FROM alerts WHERE id = %d AND news = 1 AND alert_status = "active" AND news_status = "active"'%alert_id
			memsql_objects = conn.query( q )
			conn.close()
		except:
			logging.error( "%s"%traceback.format_exc() )
			return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
			logging.error( "%s"%traceback.format_exc() )
			logging.info( return_response )
			self.write(   return_response )

		if memsql_objects == None:
			RED_ALERT = False
			return_response = json.dumps( { "status": 0, "message": "No Active Alert Found for the Specified Alert ID. Red Alert Deactivated !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password == SERVER_PASSWORD and red_alert_password == RED_ALERT_PASSWORD and wildcard_password == WILDCARD_PASSWORD:
			RED_ALERT = False
			coll.remove( { "alert_id": alert_id } )
			return_response = json.dumps( { "status": 1, "message": "Alert Obliterated !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

class WiperHandler(tornado.web.RequestHandler):
	def post(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')

		logging.info( "WiperHandler Hit Time (UTC) : %s"%datetime.datetime.utcnow() )
		logging.info( self.request ) 

		try:
			alert_id = int( self.get_argument( "alert_id", None ) )
			password =      self.get_argument( "password", None )

		except:
			logging.error( "%s"%traceback.format_exc() )
			return_response = json.dumps( { "status": 0, "message": "Error in Input." } )
			logging.error( "%s"%traceback.format_exc() )
			logging.info( return_response )
			self.write(   return_response )

		if alert_id == None:
			return_response = json.dumps( { "status": 0, "message": "No Alert ID specified." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password == None:
			return_response = json.dumps( { "status": 0, "message": "No Password specified." } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if password != SERVER_PASSWORD:
			return_response = json.dumps( { "status": 0, "message": "Wrong Password ! Access Denied !" } )
			logging.info( return_response )
			self.write(   return_response )
			return

		if alert_id == 0:
			try:
				conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
				q = 'SELECT * FROM alerts WHERE news = 1 AND alert_status = "active" AND news_status = "active"'
				memsql_objects = conn.query( q )
				conn.close()
			except:
				logging.error( "%s"%traceback.format_exc() )
				return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
				logging.error( "%s"%traceback.format_exc() )
				logging.info( return_response )
				self.write(   return_response )

			if memsql_objects == None:
				return_response = json.dumps( { "status": 0, "message": "No Active Alert Found !" } )
				logging.info( return_response )
				self.write(   return_response )
				return

			try:
				# Redis Reset for the alert_ids
				ids = []
				for memsql_object in memsql_objects:
					ids.append( memsql_object["id"] )
					json_out = json.dumps( { "alert_id": memsql_object["id"] } )
					channel1.basic_publish( exchange='', routing_key=redis_update_queue, body=json_out, properties=pika.BasicProperties( delivery_mode = 2, ) )

				return_response = json.dumps( { "status": 1, "message": "Redis Reset Successful !" } )
				logging.info( "Alert IDs Wiped - %s"%json.dumps( ids ) )
				logging.info( return_response )
				self.write(   return_response )
				return

			except:
				return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
				logging.info( return_response )
				logging.error( "%s"%traceback.format_exc() )
				self.write(   return_response )
				return

		else:
			try:
				conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
				q = 'SELECT * FROM alerts WHERE id = %d AND news = 1 AND alert_status = "active" AND news_status = "active"'%alert_id
				memsql_objects = conn.query( q )
				conn.close()
			except:
				logging.error( "%s"%traceback.format_exc() )
				return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
				logging.error( "%s"%traceback.format_exc() )
				logging.info( return_response )
				self.write(   return_response )

			if memsql_objects == None:
				return_response = json.dumps( { "status": 0, "message": "No Active Alert Found for the Specified Alert ID." } )
				logging.info( return_response )
				self.write(   return_response )
				return

			try:
				# Redis Reset for the alert_id
				json_out = json.dumps( { "alert_id": alert_id } )
				channel1.basic_publish( exchange='', routing_key=redis_update_queue, body=json_out, properties=pika.BasicProperties( delivery_mode = 2, ) )
				return_response = json.dumps( { "status": 1, "message": "Redis Reset Successful !" } )
				logging.info( return_response )
				self.write(   return_response )
				return

			except:
				return_response = json.dumps( { "status": 0, "message": "Error in Executing." } )
				logging.info( return_response )
				logging.error( "%s"%traceback.format_exc() )
				self.write(   return_response )
				return


def make_app():
	return tornado.web.Application([
		( r"/populate_alerts", MainHandler     ),
		( r"/red_alert"      , RedAlertHandler ),
		( r"/delete"         , DeletionHandler ),
		( r"/check"          , CheckHandler    ),
		( r"/wiper"          , WiperHandler    ),
	])

if __name__ == "__main__":
	app = make_app()
	app.listen( int( sys.argv[1] ) )
	tornado.ioloop.IOLoop.current().start()