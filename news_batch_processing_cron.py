#!/usr/bin/python
from __future__ import print_function
from memsql.common import database
import traceback
import datetime
import logging
import pymongo
import pdnews
import os

logging.basicConfig( filename='news_batch_processing_cron.log', level=logging.INFO, propogate=0 )

# MySQL Connection Details
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
memsql_objects = conn.query('SELECT * FROM alerts WHERE news = 1 AND alert_status = "active" AND news_status = "active"')
conn.close()

# Mongo DB Connection Details
MONGO_DB_SERVER_URL = os.environ["MONGOURL"]
DB_NAME             = "news"
COLLECTION_NAME     = "data_stream_database"

coll = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

start_date  = datetime.datetime.utcnow().replace( minute=0, second=0 ) - datetime.timedelta( hours=2 )
end_date    = datetime.datetime.utcnow().replace( minute=0, second=0 ) + datetime.timedelta( hours=6 )

line_break = "#################################################################################################################"
logging.info( "\n%s\n\nCRON Run Time : %s\nCRON Range : %s - %s\n\n%s"%( line_break, datetime.datetime.utcnow(), start_date, end_date, line_break ) )

# Obtaining the links for each alert for each hour
for item in memsql_objects:
	alert_id    = item["id"]
	uid         = item["uid"]
	publishers  = item["news_publisher"].split(",")

	optional    = process_required( item["news_kw"] )
	excluded    = process_required( item["news_ex_kw"] )

	# Runs each search terms for each query
	print( "%s - %s ### %d ### %s ### %s - "%( start_date, end_date, alert_id, optional, excluded ), end="" )
	logging.info( "%s - %s ### %d ### %s ### %s - Start"%( start_date, end_date, alert_id, optional, excluded ) )

	n = pdnews.News()

	if publishers in [ None, "", [""] ]:
		temp = n.fetch_without_publishers( start_date, end_date, optional, excluded )["newsdata"]
	else:
		temp = n.fetch_with_publishers( start_date, end_date, optional, excluded, publishers )["newsdata"]

	logging.info( "%s - %s ### %d ### %s ### %s - Count: %d"%( start_date, end_date, alert_id, optional, excluded, len( temp ) ) )
	print( "Count: %d"%( len( temp ) ) )

	if temp == []:
		pass

	else:
		for temp_item in temp:
			try:
				old_item = coll.find_one({"alert_id":alert_id, "link":temp_item["link"]})
				if old_item == None:
					temp_item["alert_id"]    = alert_id
					temp_item["uid"]         = uid
					temp_item["obtained"]    = "cron"
					temp_item["added_at"]    = datetime.datetime.utcnow()
					temp_item["key_phrase"]  = optional

					coll.insert( temp_item, check_keys=False )

				else:
					continue

			except:
				logging.error( "%s"%traceback.format_exc() )