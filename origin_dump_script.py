#!/usr/bin/python
from __future__ import print_function
from memsql.common import database
import traceback
import datetime
import pymongo
import pdnews
import os

# Loading alerts 
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
memsql_objects = conn.query('SELECT * FROM alerts WHERE news = 1 AND alert_status = "active" AND news_status = "active"')
conn.close()

# Mongo DB Connection Details
MONGO_DB_SERVER_URL = os.environ["MONGOURL"]
DB_NAME             = "news"
COLLECTION_NAME     = "data_stream_database"

coll = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

# Obtaining the links for each alert for each hour
for item in memsql_objects:
	alert_id    = item["id"]
	uid         = item["uid"]
	publishers  = item["news_publisher"].split(",")

	optional    = process_required( item["news_kw"] )
	excluded    = process_required( item["news_ex_kw"] )

	created_at  = item["created_at"]
	start_date  = created_at.replace( hour=0, minute=0, second=0 ) - datetime.timedelta( days=5 )
	end_date    = datetime.datetime.utcnow().replace( minute=0, second=0 ) + datetime.timedelta( hours=6 )

	# Runs each search terms for each query
	print( "%s - %s ### %d ### %s ### %s - "%( start_date, end_date, alert_id, optional, excluded ), end="" )

	n = pdnews.News()

	if publishers in [ None, "", [""] ]:
		temp = n.fetch_without_publishers( start_date, end_date, optional, excluded )["newsdata"]
	else:
		temp = n.fetch_with_publishers( start_date, end_date, optional, excluded, publishers )["newsdata"]

	print( "Count: %d"%( len( temp ) ) )

	if temp == []:
		pass

	else:
		for temp_item in temp:
			try:
				old_item = coll.find_one({"alert_id":alert_id, "link":temp_item["link"]})
				if old_item == None:
					temp_item["alert_id"]    = alert_id
					temp_item["uid"]         = uid
					temp_item["obtained"]    = "origin dump"
					temp_item["added_at"]    = datetime.datetime.utcnow()
					temp_item["key_phrase"]  = optional

					coll.insert( temp_item, check_keys=False )

				else:
					continue

			except:
				traceback.print_exc()