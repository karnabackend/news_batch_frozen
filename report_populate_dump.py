#!/usr/bin/python
from __future__ import print_function
from memsql.common import database
import social_media_score
import report_process
import traceback
import datetime
import requests
import pymongo
import pdnews
import json
import sys
import os

# MySQL Connection
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

# Mongo DB Connection Details
MONGO_DB_SERVER_URL         = os.environ["MONGOURL"]
DB_NAME                     = "reports"

COLLECTION_NAME_final       = "news"
COLLECTION_NAME_data_stream = "news_data_stream"

coll_final                  = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME_final       ]
coll_data_stream            = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME_data_stream ]

# Category Dictionary
news_conn = database.connect( host=HOST, user=USER, password=PASSWORD, database="news" )
Q         = "SELECT name, category, location_country, location_state from publishers"
category_objects = news_conn.query( Q )
news_conn.close()

category_dictionary = {}

for item in category_objects:
	category_dictionary[ item["name"] ] = { "category": item["category"], "location_country": item["location_country"], "location_state": item["location_state"] }

# Function to break the required query into it"s component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split( "," ):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ["", [""], []], WORDS )

def populate_report( required, excluded, days, slug_id ):
	optional    = process_required( required )
	excluded    = process_required( excluded )

	start_date  = datetime.datetime.utcnow() - datetime.timedelta( days=days )
	end_date    = datetime.datetime.utcnow()

	# Runs each search terms for each query
	print( "%s - %s ### %s ### %s - "%( start_date, end_date, optional, excluded ), end="" )

	n    = pdnews.News()
	temp = n.fetch_without_publishers( start_date, end_date, optional, excluded )

	data_stream = []

	print( "Count: %d"%( len( temp["newsdata"] ) ) )

	for temp_item in temp["newsdata"]:
		# Annotating Data
		temp_item["slug_id"] = slug_id

		temp_item["social_media_score"]                   = {}
		temp_item["social_media_score"]["fb_score"]       = social_media_score.get_fb_count(       temp_item["link"] )
		temp_item["social_media_score"]["linkedin_score"] = social_media_score.get_linkedin_count( temp_item["link"] )

		try:
			augment_pubstats         = category_dictionary[ temp_item["publisher"] ]
			temp_item["category"]         = augment_pubstats["category"]
			temp_item["location_country"] = augment_pubstats["location_country"]
			temp_item["location_state"]   = augment_pubstats["location_state"]
		except:
			temp_item["category"]         = None
			temp_item["location_country"] = None
			temp_item["location_state"]   = None

#		if temp_item.get( "ner", None ) == None:
#			try:
#				temp_item["ner"] = requests.post( "http://annotate.apis.paralleldots.com/ner", data=json.dumps( { "text": temp_item["text"] } ) ).json()["entities"]
#			except:
#				print( traceback.format_exc() )
#				temp_item["ner"] = []
#
#		if temp_item.get( "ner_heading", None ) == None:
#			try:
#				temp_item["ner_heading"] = requests.post( "http://annotate.apis.paralleldots.com/ner", data=json.dumps( { "text": temp_item["Highlight"] } ) ).json()["entities"]
#			except:
#				print( traceback.format_exc() )
#				temp_item["ner_heading"] = []
#
#		if temp_item.get( "sentimentv2", None ) == None:
#			continue
#			try:		
#				temp_item["sentimentv2"] = requests.get( "http://annotate.apis.paralleldots.com/sentiment?sentence1=" + temp_item["Highlight"] ).json()
#			except:
#				print( traceback.format_exc() )
#				temp_item["sentimentv2"] = 0.5

		data_stream.append( temp_item )
		coll_data_stream.insert( temp_item, check_keys=False )
		temp_item.pop( "_id", None )

	processed_object                           = report_process.process( start_date, end_date, data_stream )
	processed_object["topstats"]["scan_count"] = temp["scan_count"]
	processed_object["type"]                   = "news"
	processed_object["slug_id"]                = slug_id

	coll_final.insert( processed_object, check_keys=False )

	print( "Dump Completed for slug_id - %s"%slug_id )

if __name__ == "__main__":
	print( "Data Entry Sequence - required( str ), excluded( str ), start_date( str ), end_date( str )" )
	required = sys.argv[1]
	excluded = sys.argv[2]
	days     = int( sys.argv[3] )
	slug_id  = sys.argv[4]
	populate_report( required, excluded, days, slug_id )