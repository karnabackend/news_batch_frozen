#!/usr/bin/python
from memsql.common import database
import traceback
import datetime
import logging
import pdnews
import pika
import json
import os

logging.basicConfig( filename='news_live_feed.log', level=logging.INFO, propogate=0 )

# MySQL Connection Details
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
memsql_objects = conn.query('SELECT * FROM alerts WHERE news = 1 AND alert_status = "active" AND news_status = "active"')
conn.close()

# RabbitMQ Connection Details
MQ_VARIABLE = os.environ["RabbitMQ_News_Init"]			# Used to identify if the CRON is running in dev or prod
QUEUE_NAME  = "karna_feeds_news"						# Queue that the data will be pushed into

parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection = pika.BlockingConnection(parameters=parameters)
channel1   = connection.channel()
channel1.queue_declare(queue=QUEUE_NAME, durable=True)

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

start_date  = datetime.datetime.utcnow().replace( minute=0, second=0 )
end_date    = datetime.datetime.utcnow().replace( minute=0, second=0 ) + datetime.timedelta( hours=5, minutes=30 )

total = 0

logging.info( "#################################################################################################################" )
logging.info( "\nCRON Run Time (UTC)      : %s"%start_date )
logging.info( "\nCRON Run Time (UTC+5:30) : %s\n"%end_date )
logging.info( "#################################################################################################################" )

# Obtaining the links for each alert for each hour
for item in memsql_objects:
	alert_id    = item["id"]
	uid         = item["uid"]
	publishers  = item["news_publisher"].split(",")

	query       = process_required( item["news_kw"] )
	excluded    = process_required( item["news_ex_kw"] )

	if publishers in [ None, "", [""] ]:
		links = pdnews.elastic.query_without_publishers( start_date, end_date, query, excluded )
	else:
		links = pdnews.elastic.query_with_publishers( start_date, end_date, query, excluded, publishers )
	
	total = total + len( links )
	logging.info( "alert_id: %d, count: %d"%( alert_id, len( links ) ) )

	if links == []:
		pass
	else:
		for link in links:
			json_out = json.dumps( { "uid": uid, "alert_id": alert_id, "link": link } )
			channel1.basic_publish( exchange='', routing_key=QUEUE_NAME, body=json_out, properties=pika.BasicProperties( delivery_mode = 2, ) )

logging.info( "\nTotal Links Dumped - %d"%total )