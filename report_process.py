#!/usr/bin/python

__author__ = "Meghdeep Ray"

from collections import Counter
import datetime
import operator
import settings
import json

PAGINATION_DEFAULT   = settings.PAGINATION_DEFAULT
NER_LIMIT            = settings.NER_LIMIT

day_map = {}

graph_data                      = {}
graph_data["positive"]          = []
graph_data["negative"]          = []
graph_data["neutral"]           = []
graph_data["any"]               = []
graph_data["positive_data"]     = []
graph_data["negative_data"]     = []
graph_data["neutral_data"]      = []
graph_data["any_data"]          = []

publisher_data                  = {}
publisher_data["mention_count"] = {}
publisher_data["total_shares"]  = 0
publisher_data["shares_graph"]  = []

ner_map                         = {}
ner_data                        = {}
ner_data["all"]                 = []
ner_data["sentiment"]           = {}

head_ner_map                    = {}
head_ner_data                   = {}
head_ner_data["all"]            = []
head_ner_data["sentiment"]      = {}

#tax_data                        = []

def sentiment_clustering( item ):
		global graph_data
		global day_map

		day = datetime.datetime.strftime(item["timestamp"], "%b %d")
		senti_score = item["sentimentv2"]
		graph_data["any"][day_map[day]]["count"]          = graph_data["any"][day_map[day]]["count"]      + 1
		graph_data["any_data"].append( item )
		if senti_score > 0.2 and senti_score < 0.8:
			graph_data["neutral"][day_map[day]]["count"]  = graph_data["neutral"][day_map[day]]["count"]  + 1
			graph_data["neutral_data"].append( item )
			return "neutral"
		elif senti_score <= 0.2:
			graph_data["negative_data"].append( item )
			graph_data["negative"][day_map[day]]["count"] = graph_data["negative"][day_map[day]]["count"] + 1
			return "negative"
		elif senti_score >= 0.8:
			graph_data["positive_data"].append( item )
			graph_data["positive"][day_map[day]]["count"] = graph_data["positive"][day_map[day]]["count"] + 1
			return "positive"
		else:
			graph_data["neutral"][day_map[day]]["count"]  = graph_data["neutral"][day_map[day]]["count"]  + 1
			graph_data["neutral_data"].append( item )
			return "neutral"

def publisher_clustering( item, sentiment ):
	global publisher_data
	global day_map
	day = datetime.datetime.strftime( item["timestamp"], "%b %d" )

	if item.get("social_media_score", None) is not None:
		try:
			score                          = item["social_media_score"].get("linkedin_score", 0) + item["social_media_score"].get("fb_score", 0)
			publisher_data["total_shares"] = publisher_data["total_shares"] + score
		except:
			linked = item["social_media_score"].get("linkedin_score", 0)
			face   = item["social_media_score"].get("fb_score", 0)
			if type( linked )             != int:
				linked                     = 0
			if type( face )               != int:
				face                       = 0
			score                          = linked + face
			publisher_data["total_shares"] = publisher_data["total_shares"] + score
	else:
		score = 0

	item["total_total_shares"] = score

	if item["publisher"] not in publisher_data["mention_count"]:
		publisher_data["mention_count"][item["publisher"]]                     = {}
		publisher_data["mention_count"][item["publisher"]]["total"]            = 1
		publisher_data["mention_count"][item["publisher"]]["positive"]         = 0
		publisher_data["mention_count"][item["publisher"]]["negative"]         = 0
		publisher_data["mention_count"][item["publisher"]]["neutral"]          = 0
		publisher_data["mention_count"][item["publisher"]]["popular"]          = score
		publisher_data["mention_count"][item["publisher"]]["category"]         = item["category"]
		publisher_data["mention_count"][item["publisher"]]["location_country"] = item["location_country"]
		publisher_data["mention_count"][item["publisher"]]["location_state"]   = item["location_state"]
		publisher_data["mention_count"][item["publisher"]][sentiment]          = publisher_data["mention_count"][item["publisher"]][sentiment] + 1
		publisher_data["shares_graph"][day_map[day]]["mention_count"]          = publisher_data["shares_graph"][day_map[day]]["mention_count"] + 1
		publisher_data["shares_graph"][day_map[day]]["total_shares"]           = publisher_data["shares_graph"][day_map[day]]["total_shares"]  + score
	else: 
		publisher_data["mention_count"][item["publisher"]]["total"]            = publisher_data["mention_count"][item["publisher"]]["total"]   + 1
		publisher_data["mention_count"][item["publisher"]][sentiment]          = publisher_data["mention_count"][item["publisher"]][sentiment] + 1
		publisher_data["mention_count"][item["publisher"]]["popular"]          = publisher_data["mention_count"][item["publisher"]]["popular"] + score
		publisher_data["shares_graph"][day_map[day]]["mention_count"]          = publisher_data["shares_graph"][day_map[day]]["mention_count"] + 1
		publisher_data["shares_graph"][day_map[day]]["total_shares"]           = publisher_data["shares_graph"][day_map[day]]["total_shares"]  + score

def NER_clustering( item, sentiment ):
	global ner_map
	global ner_data
	current_ners = []

	if item.get("ner", None) == None:
		return
	for ners in item["ner"]:
		if ners[0].lower() in current_ners:
			continue
		else:
			current_ners.append( ners[0].lower() )
		if ners[0].lower() in ["", " ", None]:
			continue
		ner_data["all"].append( ners[0].lower() )
		if ners[0].lower() not in ner_map:
			ner_map[ners[0].lower()] = [ ners[0] ]
		else:
			ner_map[ners[0].lower()].append( ners[0] )
		if ners[0].lower() not in ner_data["sentiment"]:
			ner_data["sentiment"][ners[0].lower()]             = {}
			ner_data["sentiment"][ners[0].lower()]["positive"] = 0
			ner_data["sentiment"][ners[0].lower()]["negative"] = 0
			ner_data["sentiment"][ners[0].lower()]["neutral"]  = 0
			ner_data["sentiment"][ners[0].lower()][sentiment]  = ner_data["sentiment"][ners[0].lower()][sentiment] + 1
		else:
			ner_data["sentiment"][ners[0].lower()][sentiment]  = ner_data["sentiment"][ners[0].lower()][sentiment] + 1

def Head_NER_clustering( item, sentiment ):
	global head_ner_data
	global head_ner_map
	current_ners = []

	if item.get("ner_heading", None) == None:
		return
	for ners in item["ner_heading"]:
		if ners[0].lower() in current_ners:
			continue
		else:
			current_ners.append( ners[0].lower() )
		if ners[0].lower() in ["", " ", None]:
			continue
		head_ner_data["all"].append( ners[0].lower() )
		if ners[0].lower() not in head_ner_map:
			head_ner_map[ners[0].lower()] = [ ners[0] ]
		else:
			head_ner_map[ners[0].lower()].append( ners[0] )
		if ners[0].lower() not in head_ner_data["sentiment"]:
			head_ner_data["sentiment"][ners[0].lower()]             = {}
			head_ner_data["sentiment"][ners[0].lower()]["positive"] = 0
			head_ner_data["sentiment"][ners[0].lower()]["negative"] = 0
			head_ner_data["sentiment"][ners[0].lower()]["neutral"]  = 0
			head_ner_data["sentiment"][ners[0].lower()][sentiment]  = head_ner_data["sentiment"][ners[0].lower()][sentiment] + 1
		else:
			head_ner_data["sentiment"][ners[0].lower()][sentiment]  = head_ner_data["sentiment"][ners[0].lower()][sentiment] + 1

#def Tax_clustering( item ):
#	global tax_data
#
#	if item.get("taxonomy", None) == None or item["taxonomy"] == []:
#		return
#	try:
#		if type( item["taxonomy"] ) == unicode or type( item["taxonomy"] ) == str:
#			for taxes in json.loads( item["taxonomy"] ):
#				if taxes[0] in ["", " ", [], None]:
#					continue
#				tax_data.append( taxes[0] )
#		elif type( item["taxonomy"] ) == list:
#			for taxes in item["taxonomy"]:
#				if taxes[0] in ["", " ", [], None]:
#					continue
#				tax_data.append( taxes[0] )
#	except:
#		pass

def overall_sentiment():
	global graph_data

	total_neutral  = reduce( lambda x,y : x + y, [ k["count"] for k in graph_data["neutral" ] ] )
	total_positive = reduce( lambda x,y : x + y, [ k["count"] for k in graph_data["positive"] ] )
	total_negative = reduce( lambda x,y : x + y, [ k["count"] for k in graph_data["negative"] ] )

	if total_positive >= total_negative and total_positive > total_neutral:
		return "positive"
	elif total_negative >= total_positive and total_negative > total_neutral:
		return "negative"
	else:
		return "neutral"

def process( start_date, end_date, data ):

	global graph_data
	global publisher_data
	global ner_data
	global ner_map
	global head_ner_data
	global head_ner_map
	global tax_data
	global day_map
	global PAGINATION_DEFAULT
	global NER_LIMIT
	count = 0

	# Generating the Default Values for each Category of Graph Data
	delta = end_date - start_date
	for i in range(delta.days + 1):
		day_map[datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d")] = count
		count = count + 1
		graph_data["positive"].append({"date": datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})
		graph_data["negative"].append({"date": datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})
		graph_data["neutral"].append({"date": datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})
		graph_data["any"].append({"date": datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})
		publisher_data["shares_graph"].append({"date": datetime.datetime.strftime((start_date + datetime.timedelta(days=i)), "%b %d"), "mention_count": 0, "total_shares": 0, "shares_per_mention": 0.0})	

	# Processing the Data
	for item in data:
		sentiment = sentiment_clustering( item )
		publisher_clustering( item, sentiment )
		NER_clustering( item, sentiment )
		Head_NER_clustering( item, sentiment )
		#Tax_clustering( item )

	# Generating the Average Popularity of the Articles for each Day
	for item in publisher_data["shares_graph"]:
		if item["total_shares"] == 0:
			item["shares_per_mention"] = 0
		else:
			item["shares_per_mention"] = round( item["total_shares"] / float( item["mention_count"] ), 2 )

	# Generating Percentages for Each Sentiment Share of the News
	for publisher in publisher_data["mention_count"]:
		publisher_data["mention_count"][publisher]["positive_percentage"] = round( publisher_data["mention_count"][publisher]["positive"] / float( publisher_data["mention_count"][publisher]["total"] ), 2 )
		publisher_data["mention_count"][publisher]["negative_percentage"] = round( publisher_data["mention_count"][publisher]["negative"] / float( publisher_data["mention_count"][publisher]["total"] ), 2 )
		publisher_data["mention_count"][publisher]["neutral_percentage"]  = round( publisher_data["mention_count"][publisher]["neutral"]  / float( publisher_data["mention_count"][publisher]["total"] ), 2 )
		publisher_data["mention_count"][publisher]["shares_per_mention"]  = round( publisher_data["mention_count"][publisher]["popular"]  / float( publisher_data["mention_count"][publisher]["total"] ), 2 )

	for ner_item in ner_map:
		ner_map[ner_item] = Counter( ner_map[ner_item] ).most_common(1)[0][0]

	for ner_item in head_ner_map:
		head_ner_map[ner_item] = Counter( head_ner_map[ner_item] ).most_common(1)[0][0]

	# Building the Object
	obj                                        = {}
	obj["status"]                              = 1

	obj["topstats"]                            = {}
	obj["topstats"]["mention_count"]           = len( data )
	obj["topstats"]["overall_sentiment"]       = overall_sentiment()
	obj["topstats"]["shares_per_mention"]      = round( reduce( lambda x,y : x + y, [ k["total_shares"] for k in publisher_data["shares_graph"] ] ) / float( reduce( lambda x,y : x + y, [ k["mention_count"] for k in publisher_data["shares_graph"] ] ) ), 2 )

	obj["pubstats"]                            = publisher_data["mention_count"]

	obj["graph_data"]                          = {}
	obj["graph_data"]["sentiment"]             = {}
	obj["graph_data"]["sentiment"]["positive"] = graph_data["positive"]
	obj["graph_data"]["sentiment"]["negative"] = graph_data["negative"]
	obj["graph_data"]["sentiment"]["neutral"]  = graph_data["neutral"]
	obj["graph_data"]["sentiment"]["any"]      = graph_data["any"]
	obj["graph_data"]["shares_per_mention"]    = publisher_data["shares_graph"]
	obj["graph_data"]["average_news"]          = [ { "date": k["date"], "average_news_per_day": round( k["count"] / float( obj["topstats"]["mention_count"] ), 2 ) } for k in obj["graph_data"]["sentiment"]["any"] ]

	obj["classified_feeds"]                    = {}
	obj["classified_feeds"]["positive"]        = graph_data["positive_data"]
	obj["classified_feeds"]["negative"]        = graph_data["negative_data"]
	obj["classified_feeds"]["neutral"]         = graph_data["neutral_data"]
	obj["classified_feeds"]["popular"]         = sorted( data, key= lambda k: k["total_total_shares"], reverse= True )

	obj["popular_entities"]                    = [ { "word": ner_map[NER[0].lower()] , "count": NER[1], "sentiment": max( ner_data["sentiment"][NER[0]].iteritems(), key=operator.itemgetter(1) )[0] } for NER in Counter( ner_data["all"] ).most_common( NER_LIMIT ) ]
	obj["heading_popular_entities"]            = [ { "word": head_ner_map[NER[0].lower()] , "count": NER[1], "sentiment": max( head_ner_data["sentiment"][NER[0]].iteritems(), key=operator.itemgetter(1) )[0] } for NER in Counter( head_ner_data["all"] ).most_common( NER_LIMIT ) ]

	#obj["taxonomy"]                            = [ { "name": taxes[0], "count": taxes[1] } for taxes in Counter( tax_data ).most_common(5) ]

	obj["feeds"]                               = { "newsdata": { "feed": data[:PAGINATION_DEFAULT] } }
	
	if len( data ) <= PAGINATION_DEFAULT:
		pass
	elif len( data ) - PAGINATION_DEFAULT >= PAGINATION_DEFAULT:
		obj["feeds"]["newsdata"]["next"] = PAGINATION_DEFAULT
	#elif len( data ) < PAGINATION_DEFAULT:
	#	obj["feeds"]["newsdata"]["next"] = len( data )

	# Truncating & Trimming Classified Feeds
	for feed in obj["classified_feeds"]:
		for item in obj["classified_feeds"][feed]:
			try:
				item.pop( "ner"        , None )
				item.pop( "ner_heading", None )
				item["timestamp"] = item["timestamp"].isoformat()
			except:
				pass
		feed_length = len( obj["classified_feeds"][feed] )
		obj["classified_feeds"][feed] = { "feed": obj["classified_feeds"][feed][:PAGINATION_DEFAULT] }
		if feed_length <= PAGINATION_DEFAULT:
			pass
		elif feed_length - PAGINATION_DEFAULT    >= PAGINATION_DEFAULT:
			obj["classified_feeds"][feed]["next"] = { "skip": PAGINATION_DEFAULT, "limit": PAGINATION_DEFAULT }
		elif feed_length - PAGINATION_DEFAULT     < PAGINATION_DEFAULT:
			obj["classified_feeds"][feed]["next"] = { "skip": PAGINATION_DEFAULT, "limit": feed_length }

	# Cleaning the Global Variables
	day_map = {}

	graph_data                      = {}
	graph_data["positive"]          = []
	graph_data["negative"]          = []
	graph_data["neutral"]           = []
	graph_data["any"]               = []
	graph_data["positive_data"]     = []
	graph_data["negative_data"]     = []
	graph_data["neutral_data"]      = []
	graph_data["any_data"]          = []

	publisher_data                  = {}
	publisher_data["mention_count"] = {}
	publisher_data["total_shares"]  = 0
	publisher_data["shares_graph"]  = []

	ner_map                         = {}
	ner_data                        = {}
	ner_data["all"]                 = []
	ner_data["sentiment"]           = {}

	head_ner_map                    = {}
	head_ner_data                   = {}
	head_ner_data["all"]            = []
	head_ner_data["sentiment"]      = {}

	#tax_data                        = []

	return obj