from pybloom import ScalableBloomFilter
from sys import stdout
import threading
import datetime
import json
import time
import pika

IN_QUEUE  = "karna_feeds_news"
OUT_QUEUE = "live_feeds_news"

print( "Bloom Fliter Initialising. Prepare to Deploy !" )
time.sleep(2)
sbf = ScalableBloomFilter( mode=ScalableBloomFilter.SMALL_SET_GROWTH )
print( "Bloom Filter Intitialised ! Deployment Active !" )

print( "Current Elements in the Bloom Filter: %s"%str( sbf.count ) )
time.sleep(1)
print( "Loading saved Snapshots into Bloom Filter..." )

try:
	f = open( "snapshot.bff", "r" )
	sbf = sbf.fromfile( f )
	f.close()
	print( "Data Loaded from Snapshot !" )
except:
	print( "No Snapshot Found !" )
time.sleep(1)
print( "Current Elements in the Bloom Filter: %s"%str( sbf.count ) )

#RabbitMQ Connection Details
parameters = pika.URLParameters("amqp://ankit:root53@146.148.71.201:5672/%2F")
connection = pika.BlockingConnection( parameters=parameters )

channel1 = connection.channel()
channel2 = connection.channel()

channel1.queue_declare( queue=IN_QUEUE , durable=True )
channel2.queue_declare( queue=OUT_QUEUE, durable=True )

print( "RabbitMQ Connected !" )
print "-----------------------------------------------------------------------------------------"


def createsnapshot():
  threading.Timer( 120.0, createsnapshot ).start()
  f = open( "snapshot.bff", "w" )
  sbf.tofile( f )
  f.close()
  op = "Status: Snapshot written at %s: Elements in Bloom Filter: %s"%( str( datetime.datetime.now() ), str( sbf.count ) )
  stdout.write("\r%s" % op)
  stdout.flush()

createsnapshot()


def callback( ch, method, properties, body ):
	try:
		link     = json.loads( body )["link"]
		alert_id = json.loads( body )["alert_id"]

		key      = str( alert_id ) + link
		
		if not sbf.add( key ):
			channel2.basic_publish( exchange="", routing_key=OUT_QUEUE, body=body, properties=pika.BasicProperties( delivery_mode = 2, ) )
	
	except:
		pass

	ch.basic_ack(delivery_tag = method.delivery_tag)


channel1.basic_qos(prefetch_count=1)
channel1.basic_consume(callback,queue=IN_QUEUE)
channel1.start_consuming()