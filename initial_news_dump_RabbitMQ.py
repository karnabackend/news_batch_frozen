#!/usr/bin/python
from __future__ import print_function
from memsql.common import database
import social_media_score
import traceback
import datetime
import logging
import pymongo
import pdnews
import pika
import json
import os

logging.basicConfig( filename='initial_news_dump_RabbitMQ.log', level=logging.INFO, propogate=0 )

# RabbitMQ Connection Details
queue_name         = os.environ["RabbitMQ_News_Init"]
redis_update_queue = queue_name + "_redis_update"

parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection = pika.BlockingConnection(parameters=parameters)

channel1   = connection.channel()
channel1.queue_declare(queue=queue_name, durable=True)

channel2   = connection.channel()
channel2.queue_declare(queue=redis_update_queue, durable=True)

# MySQL Connection Details
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

# Mongo DB Connection Details
MONGO_DB_SERVER_URL = os.environ["MONGOURL"]
DB_NAME             = "news"
COLLECTION_NAME     = "data_stream_database"

coll = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

def callback(ch, method, properties, body):
	print( " [x] Received %r" % body )
	logging.info( "[x] Received - %r" % body )

	data = json.loads( body.decode('utf-8') )
	
	# Obtaining the links for each alert for each hour
	alert_id    = int( data["alert_id"] )
	uid         = data["uid"]

	conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
	#Q = 'SELECT * FROM alerts WHERE id = %d AND news = 1 AND alert_status = "active" AND news_status = "active"'%alert_id
	Q = 'SELECT * FROM alerts WHERE id = %d'%alert_id
	memsql_objects = conn.query( Q )
	conn.close()

	publishers  = memsql_objects[0]["news_publisher"].split(",")

	optional    = process_required( data["news_kw"]    )
	excluded    = process_required( data["news_ex_kw"] )

	start_date  = ( datetime.datetime.utcnow().replace( minute=0, second=0 ) - datetime.timedelta( days=2 ) ).replace( hour=0, minute=0, second=0 )
	end_date    = datetime.datetime.utcnow().replace( minute=0, second=0 ) + datetime.timedelta( hours=6 )

	# Runs each search terms for each required set
	print( "%s - %s ### %d ### %s ### %s - "%( start_date, end_date, alert_id, optional, excluded ), end="" )
	
	n = pdnews.News()

	if publishers in [ None, "", [""] ]:
		temp = n.fetch_without_publishers( start_date, end_date, optional, excluded )["newsdata"]
	else:
		temp = n.fetch_with_publishers( start_date, end_date, optional, excluded, publishers )["newsdata"]
	
	logging.info( "%s - %s ### %d ### %s ### %s - Count: %d"%( start_date, end_date, alert_id, optional, excluded, len( temp ) ) )
	print( "Count: %d"%( len( temp ) ) )

	if temp == []:
		pass

	else:
		for temp_item in temp:
			try:
				old_item = coll.find_one({"alert_id":alert_id, "link":temp_item["link"]})
				if old_item == None:
					temp_item["alert_id"]    = alert_id
					temp_item["uid"]         = uid
					temp_item["obtained"]    = "init script"
					temp_item["added_at"]    = datetime.datetime.utcnow()
					temp_item["key_phrase"]  = optional

					temp_item["social_media_score"]                   = {}
					temp_item["social_media_score"]["fb_score"]       = social_media_score.get_fb_count(       temp_item["link"] )
					temp_item["social_media_score"]["linkedin_score"] = social_media_score.get_linkedin_count( temp_item["link"] )

					coll.insert( temp_item, check_keys=False )

				else:
					continue

			except:
				logging.error( "%s"%traceback.format_exc() )

	json_out = json.dumps( { "alert_id": alert_id } )
	channel2.basic_publish( exchange='', routing_key=redis_update_queue, body=json_out, properties=pika.BasicProperties( delivery_mode = 2, ) )

	ch.basic_ack( delivery_tag = method.delivery_tag )

channel1.basic_qos( prefetch_count=1 )
channel1.basic_consume( callback, queue=queue_name )
print( ' [*] Waiting for messages. To exit press CTRL+C' )
channel1.start_consuming()