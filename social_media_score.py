#!/usr/bin/python
#from requests.packages.urllib3.exceptions import InsecureRequestWarning
import traceback
import requests
import datetime
import pymongo
import logging
import urllib
import json
import time
import os

#requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
logging.basicConfig( filename='social_media_score.log', level=logging.INFO, propogate=0 )

# Mongo DB Connection Details
MONGO_DEV_SERVER_URL  = os.environ["MONGO_DEV_URL"]
MONGO_PROD_SERVER_URL = os.environ["MONGO_PROD_URL"]
DB_NAME               = "news"
COLLECTION_NAME       = "data_stream_database"

news_dev_db           = pymongo.MongoClient( MONGO_DEV_SERVER_URL  )[ DB_NAME ][ COLLECTION_NAME ]
news_prod_db          = pymongo.MongoClient( MONGO_PROD_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# Access Tokens
fb_url                = "https://graph.facebook.com/v2.1/?%s"
access_token          = "377604329049672%7C6f09baae3e647a53d8c3f39a0103e4ac"
linkedin_url          = "https://www.linkedin.com/countserv/count/share?format=json&url=%s"

def get_fb_count(news_link):
	params = { "id": news_link, "access_token": access_token }
	try:
		url           = fb_url%urllib.urlencode( params )
		fb_response   = requests.get(url).json()

		if "share" in fb_response:
			fb_count  = fb_response['share']['comment_count'] + fb_response['share']['share_count']
		else:
			fb_count  = 0
		
		return fb_count

	except Exception as e:
		print( e )
		logging.error( "%s"%traceback.format_exc() )

def get_linkedin_count(news_link):
	try:
		url                = linkedin_url%news_link
		linkedin_response  = requests.get( url ).content
		linkedin_response  = json.loads(linkedin_response.replace('/','').replace('\\',''))

		if "count" in linkedin_response:
			linkedin_count = linkedin_response['count']
		else:
			linkedin_count = 0
		
		return linkedin_count

	except Exception as e:
		print( e )
		logging.error( "%s"%traceback.format_exc() )

def get_popularity_score():
	end_time   = datetime.datetime.utcnow()
	end_time   = end_time + datetime.timedelta( hours=5, minutes=30 )
	start_time = end_time - datetime.timedelta( hours=48 )

	print( "#############################################################################################################################" )
	print( "\nRun Time - %s\n"%str( end_time ) )

	logging.info( "#############################################################################################################################" )
	logging.info( "\nRun Time - %s\n"%str( end_time ) )

	news_links = news_prod_db.find( { "timestamp": { "$gte": start_time, "$lte": end_time } }, no_cursor_timeout=True ).distinct( "link" )
	count      = 0

	for news_link in news_links:
		try:
			dev_db_cursor  = news_dev_db.find(  { "link": news_link } )
			prod_db_cursor = news_prod_db.find( { "link": news_link } )
			count          =  count + 1
			dev_count      = 0
			prod_count     = 0
			fb_score       = get_fb_count(       news_link )
			linkedin_score = get_linkedin_count( news_link )

			print( "_____________________________________________________________________________________________________________________________" )
			print( "Count          - %d"%count          )
			print( "Link           - %s"%news_link      )
			print( "facebook score - %d"%fb_score       )
			print( "linkedin score - %d"%linkedin_score )

			for item in dev_db_cursor:
				news_dev_db.update(  { "_id": item["_id"] }, { "$set": { "social_media_score": { "fb_score": fb_score, "linkedin_score": linkedin_score } } } )
				dev_count  = dev_count  + 1

			for item in prod_db_cursor:
				news_prod_db.update( { "_id": item["_id"] }, { "$set": { "social_media_score": { "fb_score": fb_score, "linkedin_score": linkedin_score } } } )
				prod_count = prod_count + 1

			print("")
			print("Links updated in Dev  - %d"%dev_count  )
			print("Links updated in Prod - %d"%prod_count )

			logging.info( "_____________________________________________________________________________________________________________________________" )
			logging.info( "Count          - %d"%count             )
			logging.info( "Link           - %s"%news_link         )
			logging.info( "facebook score - %d"%fb_score          )
			logging.info( "linkedin score - %d"%linkedin_score    )
			logging.info( "")
			logging.info( "Links updated in Dev  - %d"%dev_count  )
			logging.info( "Links updated in Prod - %d"%prod_count )
		
		except:
			logging.error( "%s"%traceback.format_exc() )

if __name__ == '__main__':
	get_popularity_score()