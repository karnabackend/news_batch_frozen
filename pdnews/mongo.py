from pymongo import MongoClient
from .validate import accepts
from datetime import datetime
import os

MONGO_DB_SERVER_URL = os.environ["MONGO_DEV_URL"]
DB_NAME             = "client_text"
COLLECTION_NAME     = "news_data"

coll = MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

@accepts(list)
def find(links):
	resp = []
	cursor = coll.find({"link":{"$in":links}})
	for row in cursor:
		resp.append(row)
	return resp

@accepts(datetime,datetime)
def scan(startdate,enddate):
	#Scans total number of articles in a given date range
	scan_count = coll.find({"timestamp":{"$gte":startdate,"$lt":enddate}}).count()
	return scan_count

@accepts(list)
def sentiment_find(links):
	resp = []
	cursor = coll.find({ "link":{"$in":links}, "sentimentv2":{"$exists":True, "$ne":"NA"} })
	for row in cursor:
		resp.append(row)
	return resp