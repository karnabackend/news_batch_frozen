from elasticsearch import Elasticsearch
import datetime
es = Elasticsearch([ { "host": "104.154.89.51", "port": 9200 } ], timeout=30, max_retries=10, retry_on_timeout=True )

#Querying for the given keywords in Highlight, text and description
def querylinks(startdate,enddate,keywords,publinks):
	mustlist = [{"terms": {"client": publinks }}]
	for eachword in keywords:
		mustlist.append({"multi_match" : {"query": eachword, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}}) 


	q = {
	"from":0,"size":1000,
	"fields": ["link"],
	"query": {
		"filtered": {
		"query": {
			"bool": {
			"must": mustlist
			}
			},
		"filter": {
			"range": {
			"timestamp": {"gte": startdate,"lte":enddate}
				}
			}
			}
		}
		}
	data = es.search(index="nasscomnews_search",doc_type="nasscomtracking",body=q)
	return [str(obj["fields"]["link"][0]) for obj in data["hits"]["hits"]]

#Querying for the given keywords in Highlight, text and description but with support for Required, Optional and Excluded keyowrds
def complex_query_links(start_date, end_date, required, optional, excluded, publisher_links):
	
	must_list	  = []
	should_list	  = []
	must_not_list = []

	for required_word in required:
		#MOD_required_word = required_word#"\"" + required_word + "\""
		must_list.append({"multi_match" : {"query": required_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for optional_word in optional:
		#MOD_should_word = optional_word#"\"" + optional_word + "\""
		should_list.append({"multi_match" : {"query": optional_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for excluded_word in excluded:
		#MOD_must_not_word = excluded_word#"\"" + excluded_word + "\""
		must_not_list.append({"multi_match" : {"query": excluded_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})

	q = {
		"from":0,"size":1000,
		"fields": ["link"],
		"query": {
		"filtered": {
			"query": {
				"bool": {
					"must": must_list,
					"should": should_list,
					"must_not": must_not_list
					}
				},
			"filter": {
				"bool": {
					"must": [{
						"range": {
							"timestamp": {"gte": start_date,"lte":end_date}
								}
							},{
						"terms":{
							"client": publisher_links
							}
						}]
					}
				}
			}
		}
	}
	data = es.search(index="nasscomnews_search",doc_type="nasscomtracking",body=q)
	return [str(obj["fields"]["link"][0]) for obj in data["hits"]["hits"]]
	

def complex_query_links_with_scroll(start_date, end_date, required, optional, excluded, publisher_links):
	
	must_list	  = []
	should_list	  = []
	must_not_list = []

	for required_word in required:
		MOD_required_word = required_word#"\"" + required_word + "\""
		must_list.append({"multi_match" : {"query": MOD_required_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for optional_word in optional:
		MOD_should_word = optional_word#"\"" + optional_word + "\""
		should_list.append({"multi_match" : {"query": MOD_should_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for excluded_word in excluded:
		MOD_must_not_word = excluded_word#"\"" + excluded_word + "\""
		must_not_list.append({"multi_match" : {"query": MOD_must_not_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})

	q = {
		"from":0,"size":1000,
		"fields": ["link"],
		"query": {
		"filtered": {
			"query": {
				"bool": {
					"must": must_list,
					"should": should_list,
					"must_not": must_not_list
					}
				},
			"filter": {
				"bool": {
					"must": [{
						"range": {
							"timestamp": {"gte": start_date,"lte":end_date}
								}
							},{
						"terms":{
							"client": publisher_links
							}
						}]
					}
				}
			}
		}
	}
	
	page = es.search(index = "nasscomnews_search",
				doc_type = "nasscomtracking",
				scroll = "30s",
				search_type = "scan",
				size = 200,
				body = q,
				)
	sid = page["_scroll_id"]
	scroll_size = page["hits"]["total"]
	data = []
	# Start scrolling
	while (scroll_size > 0):
		print "Scrolling..."
		page = es.scroll(scroll_id = sid, scroll = "30s")
		# Update the scroll ID
		sid = page["_scroll_id"]
		# Get the number of results that we returned in the last scroll
		data = data + page["hits"]["hits"]
		#data = data + [ str(obj["fields"]["link"][0]) for line in page ]
		scroll_size = len(page["hits"]["hits"])
		print "scroll size: " + str(scroll_size)

	return [ str(obj["fields"]["link"][0]) for obj in data ]

def complex_query_links_without_publishers( start_date, end_date, required, optional, excluded ):
	
	must_list	  = []
	should_list	  = []
	must_not_list = []

	for required_word in required:
		#MOD_required_word = required_word#"\"" + required_word + "\""
		must_list.append({"multi_match" : {"query": required_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for optional_word in optional:
		#MOD_should_word = optional_word#"\"" + optional_word + "\""
		should_list.append({"multi_match" : {"query": optional_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})
	for excluded_word in excluded:
		#MOD_must_not_word = excluded_word#"\"" + excluded_word + "\""
		must_not_list.append({"multi_match" : {"query": excluded_word, "type": "phrase_prefix", "fields": [ "Highlight^2", "text", "description" ]}})

	q = {
		"from":0,"size":1000,
		"fields": ["link"],
		"query": {
		"filtered": {
			"query": {
				"bool": {
					"must": must_list,
					"should": should_list,
					"must_not": must_not_list
					}
				},
			"filter": {
				"range": {
					"timestamp": {"gte": start_date,"lte":end_date}
						}
					}
				}
			}
		}

	data = es.search(index="nasscomnews_search",doc_type="nasscomtracking",body=q)
	return [str(obj["fields"]["link"][0]) for obj in data["hits"]["hits"]]

def query_without_publishers( start_date, end_date, query, excluded ):

	def process_query_input( parameters ):
		parsed_query = []
		for parameter in parameters:
			if parameter[0] == "\"" and parameter[-1] == "\"":
				parsed_query.append( {"multi_match" : {"query": parameter[1:-1], "type": "phrase", "fields": [ "Highlight^2", "text", "description" ]}} )
			else:
				parsed_query.append( {"multi_match" : {"query": parameter, "operator": "and", "fields": [ "Highlight^2", "text", "description" ]}} )
		return parsed_query

	must_list	  = []
	should_list	  = process_query_input( query )
	must_not_list = process_query_input( excluded )

	q = {
		"from":0,"size":1000,
		"fields": ["link"],
		"query": {
		"filtered": {
			"query": {
				"bool": {
					"must": must_list,
					"should": should_list,
					"must_not": must_not_list
					}
				},
			"filter": { "range": { "timestamp": { "gte": start_date, "lte": end_date } } }
				}
			}
		}
	
	data = es.search(index="nasscomnews_search",doc_type="nasscomtracking",body=q)
	return [str(obj["fields"]["link"][0]) for obj in data["hits"]["hits"]]

def query_with_publishers( start_date, end_date, query, excluded, publisher_links ):

	def process_query_input( parameters ):
		parsed_query = []
		for parameter in parameters:
			if parameter[0] == "\"" and parameter[-1] == "\"":
				parsed_query.append( {"multi_match" : {"query": parameter[1:-1], "type": "phrase", "fields": [ "Highlight^2", "text", "description" ]}} )
			else:
				parsed_query.append( {"multi_match" : {"query": parameter, "operator": "and", "fields": [ "Highlight^2", "text", "description" ]}} )
		return parsed_query

	must_list	  = []
	should_list	  = process_query_input( query )
	must_not_list = process_query_input( excluded )

	q = {
		"from":0,"size":1000,
		"fields": ["link"],
		"query": {
		"filtered": {
			"query": {
				"bool": {
					"must": must_list,
					"should": should_list,
					"must_not": must_not_list
					}
				},
			"filter": { "bool": { "must": [
				{ "range": { "timestamp": { "gte": start_date, "lte": end_date } } },
				{ "terms": { "client": publisher_links } }
				]
			} } }
		}
	}

	data = es.search(index="nasscomnews_search",doc_type="nasscomtracking",body=q)
	return [str(obj["fields"]["link"][0]) for obj in data["hits"]["hits"]]