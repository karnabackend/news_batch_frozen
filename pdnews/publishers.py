import requests
import json

url = "http://karnadev.paralleldots.com/karna/apis/news/getpublisher"

def get(publishernames):
	resp          = requests.get(url)
	publisherlist = resp.json()
	pubdict       = {}
	for country in publisherlist:
		for item in country["publishers"]:
			try:
				pubdict[ str( item[1] ).lower()] = str( item[0] )
			except:
				pass
	return pubdict

def empty_get():
	resp          = requests.get(url)
	publisherlist = resp.json()
	pubdict       = {}
	for country in publisherlist:
		for item in country["publishers"]:
			try:
				pubdict[ str( item[1] ).lower()] = str( item[0] )
			except:
				pass
	return pubdict

def reverse_get():
	resp          = requests.get(url)
	publisherlist = resp.json()
	pubdict       = {}
	for country in publisherlist:
		for item in country["publishers"]:
			try:
				pubdict[ str( item[0] ).lower()] = str( item[1] )
			except:
				pass
	return pubdict