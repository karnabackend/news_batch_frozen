# -*- coding: utf-8 -*-

__title__     = 'ParallelDots News'
__author__    = 'Ahwan Kumar, Meghdeep Ray'
__copyright__ = 'Copyright 2016, ParallelDots'


from memsql.common import database
from pymongo import MongoClient
from collections import Counter
from datetime import datetime
import datetime
import logging
import hashlib
import redis
import json

from . import publishers
from . import elastic
from . import boltdb
from . import mongo


HOST = "memsql.paralleldots.com"
USER = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

red = redis.StrictRedis( host='104.199.138.153', port=6379, db=0 )

class NewsException(Exception):
	pass

class News(object):
	def __init__(self):
		self.is_fetched = False
		self.scan_count = 0
		self.fetched_data = None
		self.graph_data = None
		self.popularity_ratio = 0

	def fetch(self,start_date,end_date,maintag,othertags,publishernames):
		"""
		start_date : datetime object
		end_date   : datetime object
		maintag   : string
		othertags : list of strings
		publishers: list of strings
		"""
		self.start_date = start_date
		self.end_date = end_date
		self.maintag = maintag
		self.othertags = othertags
		self.publishers = publishernames
		self.datebuckets =  self.datebuckets()

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True
		# self.links = self.boltlinks()
		pubdict = publishers.get(self)
		publinks = [pubdict.get(x.lower()) for x in publishernames]

		print(start_date)
		print(end_date)
		print(maintag)
		print(publinks)

		links = elastic.querylinks(self.start_date,self.end_date,othertags,publinks)
		resp = mongo.find(links)

		return {"newsdata":resp,"scan_count":self.scan_count}

	def complex_fetch( self, start_date, end_date, required, optional, excluded, publisher_names ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		publishers : list of strings
		"""
		self.start_date = start_date
		self.end_date = end_date
		self.required = required
		self.optional = optional
		self.excluded = excluded
		self.publishers = publisher_names
		#self.datebuckets =  self.datebuckets()

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True
		# self.links = self.boltlinks()
		pubdict = publishers.get(self)
		publinks = [ y for y in [ pubdict.get(x.lower()) for x in publisher_names ] if y is not None ]
		links = elastic.complex_query_links(self.start_date, self.end_date, required, optional, excluded, publinks )
		resp = mongo.sentiment_find(links)

		response = []
		for item in resp:
			modified = {"publisher": item.get("publisher_name", None), 
				"description": item["description"], 
				"timestamp": item["timestamp"], 
				"imagelink": item["imagelink"], 
				"link": item["link"], 
				"sentimentv2": item["sentimentv2"], 
				"client": item["client"], 
				"heading": item["Highlight"], 
				"ner": item.get("ner", None), 
				"social_media_score": item.get("social_media_score", None) }
			response.append( modified )

		self.fetched_data = {"newsdata":response,"scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def complex_fetch_without_publishers( self, start_date, end_date, required, optional, excluded ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		"""
		self.start_date = start_date
		self.end_date   = end_date
		self.required   = required
		self.optional   = optional
		self.excluded   = excluded

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True

		links = elastic.complex_query_links_without_publishers( self.start_date, self.end_date, required, optional, excluded )
		resp  = mongo.sentiment_find(links)

		response = []
		for item in resp:
			modified = {"publisher": item.get("publisher_name", None),
				"description": item["description"],
				"timestamp": item["timestamp"],
				"imagelink": item["imagelink"],
				"link": item["link"],
				"sentimentv2": item["sentimentv2"],
				"client": item["client"],
				"heading": item["Highlight"],
				"ner": item.get("ner", None),
				"ner_heading": item.get("ner_heading", None),
				"social_media_score": item.get("social_media_score", { "fb_score": 0, "linkedin_score": 0 }) }
			response.append( modified )

		self.fetched_data = {"newsdata":response,"scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def fetch_without_publishers( self, start_date, end_date, query, excluded ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		"""
		self.start_date = start_date
		self.end_date   = end_date

		self.optional   = query
		self.excluded   = excluded

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True

		links = elastic.query_without_publishers( self.start_date, self.end_date, query, excluded )
		resp = mongo.sentiment_find(links)

		response = []
		for item in resp:
			modified = {"publisher": item.get("publisher_name", None),
				"description": item["description"],
				"timestamp": item["timestamp"],
				"imagelink": item.get("imagelink", None),
				"link": item["link"],
				"sentimentv2": item["sentimentv2"],
				"client": item["client"],
				"heading": item["Highlight"],
				#"taxonomy": item.get("taxonomy", None),
				"ner": item.get("ner", None),
				"ner_heading": item.get("ner_heading", None),
				"social_media_score": item.get("social_media_score", { "fb_score": 0, "linkedin_score": 0 } ) }
			response.append( modified )
		
		self.fetched_data = {"newsdata":response,"scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def fetch_with_publishers( self, start_date, end_date, query, excluded, publisher_names ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		"""
		self.start_date = start_date
		self.end_date   = end_date

		self.optional   = query
		self.excluded   = excluded

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True

		self.publishers = publisher_names

		pubdict  = publishers.empty_get()
		publinks = [ y for y in [ pubdict.get( x.lower() ) for x in publisher_names ] if y is not None ]

		links    = elastic.query_with_publishers( self.start_date, self.end_date, query, excluded, publinks )
		resp     = mongo.sentiment_find(links)

		response = []
		for item in resp:
			modified = {"publisher": item.get("publisher_name", None),
				"description": item["description"],
				"timestamp": item["timestamp"],
				"imagelink": item.get("imagelink", None),
				"link": item["link"],
				"sentimentv2": item["sentimentv2"],
				"client": item["client"],
				"heading": item["Highlight"],
				#"taxonomy": item.get("taxonomy", None),
				"ner": item.get("ner", None),
				"ner_heading": item.get("ner_heading", None),
				"social_media_score": item.get("social_media_score", { "fb_score": 0, "linkedin_score": 0 } ) }
			response.append( modified )
		
		self.fetched_data = {"newsdata":response,"scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def fetch_without_publishers_without_sentimentv2( self, start_date, end_date, query, excluded ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		"""
		self.start_date = start_date
		self.end_date   = end_date

		self.optional   = query
		self.excluded   = excluded

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True

		links = elastic.query_without_publishers( self.start_date, self.end_date, query, excluded )
		resp  = mongo.find( links )

		response = []
		for item in resp:
			response.append( item )

		self.fetched_data = {"newsdata":response, "scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def complex_fetch_with_scroll( self, start_date, end_date, required, optional, excluded, publisher_names ):
		"""
		start_date : datetime object
		end_date   : datetime object
		required   : list of strings
		optional   : list of strings
		excluded   : list of strings
		publishers : list of strings
		"""
		self.start_date = start_date
		self.end_date = end_date
		self.required = required
		self.optional = optional
		self.excluded = excluded
		self.publishers = publisher_names
		#self.datebuckets =  self.datebuckets()

		self.scan_count = mongo.scan(start_date,end_date)
		self.is_fetched = True
		# self.links = self.boltlinks()
		pubdict = publishers.get(self)
		publinks = [ y for y in [ pubdict.get(x.lower()) for x in publisher_names ] if y is not None ]
		links = elastic.complex_query_links_with_scroll(self.start_date, self.end_date, required, optional, excluded, publinks )
		resp = mongo.sentiment_find(links)

		response = []
		for item in resp:
			modified = {"publisher": item.get("publisher_name", None), 
				"description": item["description"], 
				"timestamp": item["timestamp"], 
				"imagelink": item["imagelink"], 
				"link": item["link"], 
				"sentimentv2": item["sentimentv2"], 
				"client": item["client"], 
				"heading": item["Highlight"], 
				"ner": item.get("ner", None), 
				"social_media_score": item.get("social_media_score", None) }
			response.append( modified )
		
		self.fetched_data = {"newsdata":response,"scan_count":self.scan_count}
		return {"newsdata":response,"scan_count":self.scan_count}

	def datebuckets(self):
		datebuckets = boltdb.datebuckets(self.start_date,self.end_date)
		return datebuckets

	def boltlinks(self):
		return boltdb.boltlinks(self.maintag,self.datebuckets)

	def process(self):
		if not self.is_fetched:
			print('You must `fetch()` news before '
				  'calling `process()` on it!')
			raise NewsException()

		print(self.scan_count)


	def sentiment_cluster( self ):
		def incrementer( senti, day ):
			for dictionary in self.graph_data[senti]:
				if dictionary["date"] == day:
					dictionary["count"] = dictionary["count"] + 1

		if self.fetched_data == None:
			print("You must call `complex_fetch()` on news before calling `sentiment_cluster()` on it!")
			raise NewsException()

		data = self.fetched_data["newsdata"]
		positive_data = []
		negative_data = []
		neutral_data  = []
		obj = {}
		obj["positive"] = {}
		obj["negative"] = {}
		obj["any"] = {}

		self.graph_data             = {}
		self.graph_data["positive"] = []
		self.graph_data["negative"] = []
		self.graph_data["any"]      = []

		delta = self.end_date - self.start_date
		for i in range(delta.days + 1):
			self.graph_data["positive"].append({"date": datetime.datetime.strftime((self.start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0}) 
			self.graph_data["negative"].append({"date": datetime.datetime.strftime((self.start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})
			self.graph_data["any"].append({"date": datetime.datetime.strftime((self.start_date + datetime.timedelta(days=i)), "%b %d"), "count": 0})

		for item in data:
			#try:
			day = datetime.datetime.strftime(item["timestamp"], "%b %d")
			senti_score = item["sentimentv2"]
			incrementer("any", day)
			if senti_score > 0.2 and senti_score < 0.8:
				neutral_data.append( item )
			elif senti_score <= 0.2:
				negative_data.append( item )
				incrementer("negative", day)
			elif senti_score >= 0.8:
				positive_data.append( item )
				incrementer("positive", day)
			else:
				neutral_data.append( item )
			try:    
				if item.get("social_media_score", None) is not None:
					self.popularity_ratio = self.popularity_ratio + item["social_media_score"].get("linkedin_score", 0) + item["social_media_score"].get("fb_score", 0)
			except:
				pass

		#self.graph_data             = {}
		#self.graph_data["positive"] = sorted([ { "date": x,"count": obj["positive"][x] } for x in obj["positive"].keys() ], key=lambda k:datetime.datetime.strptime(k["date"], "%b %d %y"))
		#self.graph_data["negative"] = sorted([ { "date": x,"count": obj["negative"][x] } for x in obj["negative"].keys() ], key=lambda k:datetime.datetime.strptime(k["date"], "%b %d %y"))
		#self.graph_data["any"]      = sorted([ { "date": x,"count": obj["any"][x] } for x in obj["any"].keys() ], key=lambda k:datetime.datetime.strptime(k["date"], "%b %d %y"))

#        for item in self.graph_data["positive"]:
#            item["date"] = item["date"][:-3]
#        for item in self.graph_data["negative"]:
#            item["date"] = item["date"][:-3]
#        for item in self.graph_data["any"]:
#            item["date"] = item["date"][:-3]

		return {"positive_data": positive_data, "negative_data": negative_data, "neutral_data": neutral_data}

	def client_cluster( self ):
		if self.fetched_data == None:
			print("You must call `complex_fetch()` on news before calling `sentiment_cluster()` on it!")
			raise NewsException()

		data = self.fetched_data["newsdata"]
		long_names = dict(Counter([ x["client"] for x in data ]).most_common())
		reverse_pub_links = publishers.reverse_get()
		output = {}
		for item in long_names:
			output[reverse_pub_links[item]] = long_names[item]
		return output


	def average_mentions( self, client_cluster_obj ):
		if ( self.end_date - self.start_date ).days == 0:
			return client_cluster_obj
		
		days = ( self.end_date - self.start_date ).days
		r_obj = {}
		for item in client_cluster_obj:
			r_obj[item] = client_cluster_obj[item] / days
		return r_obj

	def overall_sentiment( self, sentiment_cluster_obj ):
		positive_count = len(sentiment_cluster_obj["positive_data"])
		negative_count = len(sentiment_cluster_obj["negative_data"])
		neutral_count = len(sentiment_cluster_obj["neutral_data"])
		if positive_count > negative_count and positive_count > neutral_count:
			return "positive"
		elif negative_count > positive_count and negative_count > neutral_count:
			return "negative"
		else:
			return "neutral"

	def popularity_score(self):
		if self.fetched_data == None:
			print("You must call `complex_fetch()` on news before calling `sentiment_cluster()` on it!")
			raise NewsException()

		data = self.fetched_data["newsdata"]
		obj = []
		for item in data:
			if item.get("social_media_score", None) != None:
				if item.get("social_media_score", {}).get("linkedin_score",0) != None and item.get("social_media_score", {}).get("fb_score",0) != None:
					obj.append(item)
		if obj == []:
			return []
		else:
			return sorted( obj, key=lambda k : k["social_media_score"]["linkedin_score"] + k["social_media_score"]["fb_score"], reverse=True )[:20]


	def datebuckets(self):
		datebuckets = boltdb.datebuckets(self.start_date,self.end_date)
		return datebuckets

	def boltlinks(self):
		return boltdb.boltlinks(self.maintag,self.datebuckets)

	def process(self):
		if not self.is_fetched:
			print('You must `fetch()` news before '
				  'calling `process()` on it!')
			raise NewsException()

		print(self.scan_count)

	def number_of_sources( self, client_cluster_obj ):
		return len(client_cluster_obj)

	def most_common_ners( self ):
		if self.fetched_data == None:
			print("You must call `complex_fetch()` on news before calling `sentiment_cluster()` on it!")
			raise NewsException()

		data = self.fetched_data["newsdata"]
		ner_list = []
		for item in data:
			if item.get("ner", None) == None:
				continue
			for ners in item["ner"]:
				if ners[0] in ["", " ", None]:
					continue
				ner_list.append(ners[0])
		most_comm = Counter( ner_list ).most_common(25)
		return [(x[0].encode('utf-8'), x[1]) for x in most_comm]


	def complex_process(self):
		if not self.is_fetched:
			print('You must `complex_fetch()` news before '
				  'calling `process()` on it!')
			raise NewsException()

		uhex = hashlib.sha224( ';'.join(map(str, [ self.start_date, self.end_date, self.required, self.optional, self.excluded, self.publishers ])) ).hexdigest()
		output = {}

		
		output["popularentities"]              = {}
		for item in self.most_common_ners():
			output["popularentities"][item[0]] = item[1]

		for item in self.fetched_data["newsdata"]:
			item.pop("ner", None)

		self.fetched_data["newsdata"] = filter(lambda k:k["timestamp"] != None, self.fetched_data["newsdata"])
		self.fetched_data["newsdata"] = sorted( self.fetched_data["newsdata"], key=lambda k: k["timestamp"], reverse=True )
		sentiment_object                       = self.sentiment_cluster()
		average_news = []
		try:
			av_denominator = reduce( lambda x,y: x+y, [ k["count"] for k in self.graph_data["any"] ] )
		except:
			return {"status": 0, "message": "No data found !"}
		try:
			for item in self.graph_data["any"]:
				average_news.append({ "date": item["date"], "average_news_per_day": round(item["count"]/float(av_denominator), 6) })
		except:
			return {"status": 0, "message": "No data found !"}

		output["status"]                       = 1
		output["pubstats"]                     = self.client_cluster()
		output["topstats"]                     = {}
		output["topstats"]["avg_sentiment"]    = self.overall_sentiment( sentiment_object )
		output["topstats"]["scan_count"]       = self.scan_count
		output["topstats"]["sources_count"]    = len( self.publishers )
		output["topstats"]["mention_count"]    = len( self.fetched_data["newsdata"] )
		output["topstats"]["popularity_ratio"] = round( self.popularity_ratio / float( output["topstats"]["mention_count"] ), 2 )
		output["topstats"]["average_news"]     = average_news
		output["graphdata"]                    = self.graph_data
		output["classified_feeds"]             = {}
		try:
			output["classified_feeds"]["positive"] = {"newsdata": sorted(sentiment_object["positive_data"], key=lambda k:k["timestamp"], reverse=True)}
		except:
			output["classified_feeds"]["positive"] = {"newsdata": sentiment_object["positive_data"]}
		try:
			output["classified_feeds"]["negative"] = {"newsdata": sorted(sentiment_object["negative_data"], key=lambda k:k["timestamp"], reverse=True)}
		except:
			output["classified_feeds"]["positive"] = {"newsdata": sentiment_object["negative_data"]}
		#try:
		#    output["classified_feeds"]["popular"]  = {"newsdata": sorted(self.popularity_score(), key=lambda k:k["timestamp"], reverse=True)}
		#except:
		output["classified_feeds"]["popular"]  = {"newsdata": self.popularity_score()}

		self.fetched_data["newsdata"] = sorted( self.fetched_data["newsdata"], key=lambda k: k["timestamp"], reverse=True )
		for item in self.fetched_data["newsdata"]:
			item["timestamp"] = item["timestamp"].isoformat()
			try:
				item.pop("_id")
			except:
				pass

		output["feeds"] = {"newsdata": self.fetched_data["newsdata"][:20]}
		output["feeds"]["next"] = "http://utilities.paralleldots.com/news2.0?next="+uhex+"&skip=20&limit=20"
		output["total"] = output["topstats"]["mention_count"]
		#except:
		#    output["status"] = 0
		for item in output["classified_feeds"]["positive"]["newsdata"]:
			try:
				item.pop("_id")
			except:
				pass
		for item in output["classified_feeds"]["negative"]["newsdata"]:
			try:
				item.pop("_id")
			except:
				pass



		red.set( uhex, json.dumps(self.fetched_data["newsdata"]), 3*60*60 )
		return output

	def customize_with_uid( self, uid ):
		conn = database.connect(host=HOST,user=USER, password=PASSWORD, database=DATABASE)
		favlinks = []
		archivelinks = []
		favourites_data = conn.query('SELECT * FROM favourites WHERE user_id=%s',uid)
		favlinks = [json.loads(fav['data'])['link'] for fav in favourites_data]
		favdict = {}
		for fav in favourites_data:
			fd = json.loads(fav['data'])
			favdict[fd['link']]=fav['id']
		archive_data = conn.query('SELECT * FROM archive WHERE user_id=%s',uid)
		conn.close()

		response = []
		for item in self.fetched_data["newsdata"]:
			modified = item
			#if modified["link"] in favlinks:
			if modified["link"] in favlinks:
				modified["favorite"] = 1
				modified["fid"] = favdict[modified["link"]]
			else:
				modified["favorite"] = 0

			if modified["link"] in archivelinks:
				modified["archive"] = 1
			else:
				modified["archive"] = 0

			response.append( modified )
		
		response = sorted( response, key=lambda k:k["timestamp"], reverse=True)
		self.fetched_data["newsdata"] = response

	def complex_process_for_cron(self):
		def remove_dots(data):
			for key in data.keys():
				if type(data[key]) is dict: data[key] = remove_dots(data[key])
				if '.' in key:
					data[key.replace('.', '\uff0E')] = data[key]
					del data[key]
			return data

		if not self.is_fetched:
			print('You must `complex_fetch()` news before '
				  'calling `process()` on it!')
			raise NewsException()

		uhex = hashlib.sha224( ';'.join(map(str, [ self.start_date, self.end_date, self.required, self.optional, self.excluded, self.publishers ])) ).hexdigest()
		output = {}

		
		output["popularentities"]              = {}
		for item in self.most_common_ners():
			output["popularentities"][item[0]] = item[1]
		output["popularentities"] = remove_dots( output["popularentities"] )

		for item in self.fetched_data["newsdata"]:
			item.pop("ner", None)

		self.fetched_data["newsdata"] = filter(lambda k:k["timestamp"] != None, self.fetched_data["newsdata"])
		self.fetched_data["newsdata"] = sorted( self.fetched_data["newsdata"], key=lambda k: k["timestamp"], reverse=True )
		sentiment_object                       = self.sentiment_cluster()
		average_news = []
		try:
			av_denominator = reduce( lambda x,y: x+y, [ k["count"] for k in self.graph_data["any"] ] )
		except:
			return {"status": 0, "message": "No data found !"}
		try:
			for item in self.graph_data["any"]:
				average_news.append({ "date": item["date"], "average_news_per_day": round(item["count"]/float(av_denominator), 6) })
		except:
			return {"status": 0, "message": "No data found !"}

		output["status"]                       = 1
		output["cached"]                       = self.fetched_data["newsdata"]
		output["pubstats"]                     = remove_dots( self.client_cluster() )
		output["topstats"]                     = {}
		output["topstats"]["avg_sentiment"]    = self.overall_sentiment( sentiment_object )
		output["topstats"]["scan_count"]       = self.scan_count
		output["topstats"]["sources_count"]    = len( self.publishers )
		output["topstats"]["mention_count"]    = len( self.fetched_data["newsdata"] )
		output["topstats"]["popularity_ratio"] = round( self.popularity_ratio / float( output["topstats"]["mention_count"] ), 2 )
		output["topstats"]["average_news"]     = average_news
		output["graphdata"]                    = self.graph_data
		output["classified_feeds"]             = {}
		try:
			output["classified_feeds"]["positive"] = {"newsdata": sorted(sentiment_object["positive_data"], key=lambda k:k["timestamp"], reverse=True)}
		except:
			output["classified_feeds"]["positive"] = {"newsdata": sentiment_object["positive_data"]}
		try:
			output["classified_feeds"]["negative"] = {"newsdata": sorted(sentiment_object["negative_data"], key=lambda k:k["timestamp"], reverse=True)}
		except:
			output["classified_feeds"]["positive"] = {"newsdata": sentiment_object["negative_data"]}
		#try:
		#    output["classified_feeds"]["popular"]  = {"newsdata": sorted(self.popularity_score(), key=lambda k:k["timestamp"], reverse=True)}
		#except:
		output["classified_feeds"]["popular"]  = {"newsdata": self.popularity_score()}

		self.fetched_data["newsdata"] = sorted( self.fetched_data["newsdata"], key=lambda k: k["timestamp"], reverse=True )
		for item in self.fetched_data["newsdata"]:
			item["timestamp"] = item["timestamp"].isoformat()
			try:
				item.pop("_id")
			except:
				pass

#        temp = 
#        for item in output["pubstats"]:
#            item = item.replace('.', '\uff0E')

		output["feeds"] = {"newsdata": self.fetched_data["newsdata"][:20]}
		#output["feeds"]["next"] = "http://utilities.paralleldots.com/news2.0?next="+uhex+"&skip=20&limit=20"
		output["total"] = output["topstats"]["mention_count"]
		#except:
		#    output["status"] = 0
		for item in output["classified_feeds"]["positive"]["newsdata"]:
			try:
				item.pop("_id")
			except:
				pass
		for item in output["classified_feeds"]["negative"]["newsdata"]:
			try:
				item.pop("_id")
			except:
				pass


		#red.set( uhex, json.dumps(self.fetched_data["newsdata"]), 3*60*60 )
		return output