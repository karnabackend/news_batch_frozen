import requests
import math
from datetime import datetime
import json
from .validate import accepts

bolturl = "http://130.211.140.78:1112/get"

@accepts(datetime,datetime)
def datebuckets(startdate,enddate):
	startyear = startdate.year
	startqtr = int(math.ceil(startdate.month/float(3)))
	endyear = enddate.year
	endqtr = int(math.ceil(enddate.month/float(3)))
	bucketlist = []
	for year in range(startyear,endyear+1):
		if startyear == endyear:
			while startqtr <= endqtr:
				bucketlist.append(str(year)+"Q"+str(startqtr))
				startqtr +=1
		elif endyear > startyear:
			if year == startyear:
				while startqtr <=4:
					bucketlist.append(str(year)+"Q"+str(startqtr))
					startqtr +=1
			elif year < endyear:
				startqtr = 1
				while startqtr <=4:
					bucketlist.append(str(year)+"Q"+str(startqtr))
					startqtr +=1
			elif year == endyear:
				startqtr = 1
				while startqtr <= endqtr:
					bucketlist.append(str(year)+"Q"+str(startqtr))
					startqtr +=1
	return bucketlist

@accepts(str,list)
def boltlinks(tag,bucketlist):
	links = []
	for datebucket in bucketlist:
		params = {'tag':tag,"date_bucket":datebucket}
		resp = requests.get(bolturl,params=params)
		links.extend(resp.json())
	return links
