#!/usr/bin/python
from memsql.common import database
import datetime
import logging
import pymongo
import hashlib
import redis
import json
import sys
import os

# Loading alerts 
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

# MySQL Connection and Query
conn = database.connect( host=HOST, user=USER, password=PASSWORD, database=DATABASE )
memsql_objects = conn.query( 'SELECT * FROM alerts WHERE news = 1 AND alert_status = "active"' )
conn.close()

# Additional Variable to maintain decoupling between dev & prod
queue_name = os.environ["RabbitMQ_News_Init"]

# Mongo DB Connection
MONGO_DB_SERVER_URL = os.environ["MONGOURL"]
DB_NAME             = "news"
COLLECTION_NAME     = "data_stream_database"

coll = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME ]

# Redis Connection
red = redis.StrictRedis( host='104.199.138.153', port=6379, db=0 )

# Logging Configuration
logging.basicConfig( filename='redis_caching.log', level=logging.INFO, propogate=0 )
print( "\nRun Time (UTC)      - %s"%datetime.datetime.utcnow() )
print( "Run Time (UTC+5:30) - %s\n"%( datetime.datetime.utcnow() + datetime.timedelta( hours=5, minutes=30 ) ) )
logging.info( "\nRun Time (UTC)      - %s"%datetime.datetime.utcnow() )
logging.info( "Run Time (UTC+5:30) - %s\n"%( datetime.datetime.utcnow() + datetime.timedelta( hours=5, minutes=30 ) ) )

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data in [ None, "" ]:
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

# Start Dates for the 1 day, 2 days, 7 days & 30 days buckets
end_date    = datetime.datetime.utcnow() + datetime.timedelta( hours=5, minutes=30 )
start_dates = [ ( end_date - datetime.timedelta( days=x ) ).replace( hour=0, minute=0, second=0 ) for x in [ 1, 2, 6, 29 ] ]

# Running Queries for Date Buckets of 1 day, 2 days, 7 days & 30 days
for item in memsql_objects:
	alert_id    = item["id"]
	uid         = item["uid"]

	optional    = process_required( item["news_kw"   ] )
	excluded    = process_required( item["news_ex_kw"] )

	publishers  = item["news_publisher"]
	publishers = [ x for x in publishers.encode('utf-8').split(",") ]

	# Runs each search terms for each query
	print( "_____________________________________________________________________________________________________________________________" )
	print( "Alert ID       - %d"%alert_id )
	print( "Search Terms   - %s"%optional )
	print( "Excluded Terms - %s"%excluded )

	logging.info( "_____________________________________________________________________________________________________________________________" )
	logging.info( "Alert ID       - %d"%alert_id )
	logging.info( "Search Terms   - %s"%optional )
	logging.info( "Excluded Terms - %s"%excluded )

	for start_date in start_dates:
		data = coll.find( { "alert_id": alert_id, "timestamp": { "$gte": start_date, "$lt": end_date }, "publisher": { "$in": publishers } } ).batch_size( 2000 )
		logging.info( "%s - %s ### Count: %d"%( start_date, end_date, data.count() ) )
		print( "%s - %s ### Count: %d"%( start_date, end_date, data.count() ) )

		built_object = []
		for item in data:
			item["timestamp"] = item["timestamp"].isoformat()
			item.pop( "_id"     , None )
			item.pop( "obtained", None )
			item.pop( "added_at", None )
			built_object.append( item )

		uhex = hashlib.sha224( queue_name + str( alert_id ) + datetime.datetime.strftime( start_date, "%Y-%m-%d" ) + datetime.datetime.strftime( end_date, "%Y-%m-%d" ) ).hexdigest()
		red.set( uhex, json.dumps( built_object ), 1*60*60 )