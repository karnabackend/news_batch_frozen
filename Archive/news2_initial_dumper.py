#!/usr/bin/python
from memsql.common import database
import datetime
import pymongo
import pdnews
import pika
import json

# Loading alerts 
parameters = pika.URLParameters('amqp://ankit:root53@146.148.71.201:5672/%2F')
connection = pika.BlockingConnection(parameters=parameters)
channel1 = connection.channel()
channel1.queue_declare(queue='news2.0', durable=True)

coll = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["data_stream_database"]

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data == "":
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORD = []
		if word[-1] == "\"" and word[0] == "\"":
			WORD.append(word)
		else:
			WORD = WORD + word.split(" ")
		WORDS.append( WORD )
		#WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

def callback(ch, method, properties, body):
	print(" [x] Received %r" % body)
	data = json.loads(body.decode('utf-8'))
	
	# Obtaining the links for each alert for each hour
	alert_id    = data["alert_id"]
	uid         = data["uid"]
	required    = process_required( data["keyword"] )
	optional    = []
	excluded    = []#data["excluded_kw"]
	start_date  = ( datetime.datetime.utcnow().replace( minute=0, second=0 ) - datetime.timedelta( days=2 ) ).replace( hour=0, minute=0, second=0 )
	end_date    = datetime.datetime.utcnow().replace( minute=0, second=0 ) + datetime.timedelta( hours=6 )

	# Runs each search terms for each required set
	for term in required:
		print start_date, end_date, term, optional, excluded
		n = pdnews.News()
		temp = n.complex_fetch_without_publishers( start_date, end_date, term, optional, excluded )["newsdata"]
		for temp_item in temp:
			temp_item["key_phrase"] = [ " ".join( term ) ]
		if temp == []:
			pass
		else:
			for temp_item in temp:
				try:
					old_item = coll.find_one({"alert_id":alert_id, "link":temp_item["link"]})
					if old_item != None:
							if temp_item["key_phrase"][0] not in old_item["key_phrase"]:
								old_item["key_phrase"] = old_item["key_phrase"] + temp_item["key_phrase"]
								coll.update({"_id": old_item["_id"]}, {"$set": {"key_phrase": old_item["key_phrase"] }}, check_keys=False )
							
							else:
								continue

					else:
						temp_item["alert_id"]    = alert_id
						temp_item["uid"]         = uid

						coll.insert( temp_item, check_keys=False )

				except:
					traceback.print_exc()
	ch.basic_ack(delivery_tag = method.delivery_tag)

channel1.basic_qos(prefetch_count=1)
channel1.basic_consume(callback,queue='news2.0')
print(' [*] Waiting for messages. To exit press CTRL+C')
channel1.start_consuming()