import pymongo

coll_omega = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["data_stream_database"]

data = coll_omega.find({}).batch_size(500)

for item in data:
	coll_omega.update({"_id": item["_id"]}, {"$set": {"key_phrase": list( set( item["key_phrase"] )  ) }}, check_keys=False )