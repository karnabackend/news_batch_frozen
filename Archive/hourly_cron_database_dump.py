#!/usr/bin/python
from memsql.common import database
import traceback
import datetime
import pymongo
import pdnews

# Loading alerts 
HOST = "memsql.paralleldots.com"
USER = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

conn = database.connect(host=HOST,user=USER, password=PASSWORD, database=DATABASE)
memsql_objects = conn.query('SELECT * FROM myalerts')
conn.close()

coll = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["data_stream_database"]

# Function to break the required query into it's component parts
def process_required( string_data ):
	if string_data == "":
		return []
	WORDS = []
	for word in string_data.split(","):
		word = word.strip()
		WORD = []
		if word[-1] == "\"" and word[0] == "\"":
			WORD.append(word)
		else:
			WORD = WORD + word.split(" ")
		WORDS.append( WORD )
		#WORDS.append( word )
	return filter( lambda k: k not in ['', [""], []], WORDS )

end_dates = []
for i in range(14, 18):
	for j in range(0,24):
		end_dates.append( datetime.datetime(2016,12,i,j) )

for end_date in end_dates:
	print end_date
	# Obtaining the links for each alert for each hour
	for item in memsql_objects:
		alert_id    = item["id"]
		uid         = item["uid"]
		required    = process_required( item["keyword"] )
		optional    = []
		if item["excluded_kw"] is None:
			excluded    = []
		else:
			excluded    = item["excluded_kw"]
		start_date  = end_date - datetime.timedelta( hours=1 )#datetime.datetime.utcnow().replace( hour=0, minute=0, second=0 ) - datetime.timedelta(hours=1)
		#end_date    = #datetime.datetime.utcnow().replace( hour=0, minute=0, second=0 )

		# Runs each search terms for each required set
		for term in required:
			print start_date, end_date, term, optional, excluded
			n = pdnews.News()
			temp = n.complex_fetch_without_publishers( start_date, end_date, term, optional, excluded )["newsdata"]
			for temp_item in temp:
				temp_item["key_phrase"] = [ " ".join( term ) ]
			if temp == []:
				pass
			else:
				for temp_item in temp:
					try:
						old_item = coll.find_one({"alert_id":alert_id, "link":temp_item["link"]})
						if old_item != None:
								if temp_item["key_phrase"][0] not in old_item["key_phrase"]:
									old_item["key_phrase"] = old_item["key_phrase"] + temp_item["key_phrase"]
									coll.update({"_id": old_item["_id"]}, {"$set": {"key_phrase": old_item["key_phrase"] }}, check_keys=False )
								
								else:
									continue

						else:
							temp_item["alert_id"]    = alert_id
							temp_item["uid"]         = uid

							coll.insert( temp_item, check_keys=False )

					except:
						traceback.print_exc()
						#temp_item["alert_id"]    = alert_id
						#temp_item["uid"]         = uid
#
						#coll.insert( temp_item, check_keys=False )