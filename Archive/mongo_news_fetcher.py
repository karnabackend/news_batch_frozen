from memsql.common import database
import tornado.ioloop
import tornado.web
import datetime
import pymongo
import hashlib
import pdnews
import redis
import json
import sys

HOST = "memsql.paralleldots.com"
USER = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"

coll = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["client_data"]
red = redis.StrictRedis(host='104.199.138.153', port=6379, db=0)

class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		alert_id     = int( self.get_argument("alert_id", None) )
		date_bucket  = self.get_argument("date_bucket", None)
		date_buckets = [ "12_hours", "24_hours", "2_days", "7_days" ]
		if date_bucket not in date_buckets:
			self.write( {"status": 0, "message": "Invalid Date Bucket"} )

		try:
			news_obj = coll.find({"alert_id":alert_id})[0]
			if len( news_obj["data"][date_bucket]["feeds"]["newsdata"] ) == 20:
				news_obj["data"][date_bucket]["feeds"]["next"] = "http://utilities.paralleldots.com/news/feed?alert_id=%d&date_bucket=%s&skip=%d&limit=%d"%(alert_id,date_bucket,20,20)
			self.write( news_obj["data"][date_bucket] )

		except IndexError:
			#self.write( {"status": 0, "message": "Invalid alert_id"} )
			# Loading alerts 
			HOST = "memsql.paralleldots.com"
			USER = "ankit"
			PASSWORD = "mongodude123"
			DATABASE = "socialmedia"
			conn = database.connect(host=HOST,user=USER, password=PASSWORD, database=DATABASE)
			memsql_query = 'SELECT * FROM myalerts WHERE id=%d'%alert_id
			memsql_objects = conn.query( memsql_query )
			conn.close()


			def process( string_data ):
				if string_data == "":
					return []
				WORD = []
				for word in string_data.split(","):
					if word[-1] == "\"" and word[0] == "\"":
						WORD.append(word)
					else:
						WORD = WORD + word.split(" ")
				return filter( lambda k: k != '', WORD )

			valid_keys = pdnews.publishers.empty_get().keys()
			try:
				item = memsql_objects[0]

				uid         = item["uid"]
				required    = process( item["keyword"] )
				optional    = process( item["optional_kw"] )
				excluded    = []
				#excluded    = process( item["excluded_kw"] )
				publishers  = [ x for x in item["publisher"].split(",") ]
				temp = []

				timedeltas = { "12_hours": datetime.timedelta(hours=12), "24_hours": datetime.timedelta(days= 1), "2_days": datetime.timedelta(days=2), "7_days": datetime.timedelta(days=7) }
				if date_bucket not in timedeltas.keys():
					self.write( {"status": 0, "message": "Invalid Date Bucket"} )
				end_date   = datetime.datetime.utcnow().replace(minute=0, second=0)
				start_date = datetime.datetime.utcnow().replace(minute=0, second=0) - timedeltas[date_bucket]

				for publisher in publishers:
					if publisher.lower() not in valid_keys:
						continue
					else:
						temp.append(publisher)
				publishers = temp
				alert_id = item["id"]

				obj = {}

				n = pdnews.News()
				n.complex_fetch( start_date, end_date, required, optional, excluded, publishers )
				n.customize_with_uid(uid)
				output = n.complex_process_for_cron()
				if output["cached"] > 20:
					uhex = hashlib.sha224( str( alert_id ) + date_bucket ).hexdigest()
					red.set( uhex, json.dumps( output.pop("cached") ), 3*60*60 )
					output["feeds"]["next"] = "http://utilities.paralleldots.com/news/feed?alert_id=%d&date_bucket=%s&skip=%d&limit=%d"%(alert_id,date_bucket,20,20)
				self.write( output )

			except IndexError:
				self.write( {"status": 0, "message": "Invalid alert_id"} )

		except KeyError:
			self.write( news_obj["data"][date_bucket] )

		except Exception as e:
			print(e)
			self.write( {"status": 0, "message": "Oops Something Went Wrong"} )

class FeedHandler(tornado.web.RequestHandler):
	def get(self):
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		alert_id     = int( self.get_argument("alert_id", None) )
		date_bucket  = self.get_argument("date_bucket", None)
		skip         = int( self.get_argument("skip", 20) )
		limit        = int( self.get_argument("limit", 20) )
		if limit > 20:
			limit = 20
		date_buckets = [ "12_hours", "24_hours", "2_days", "7_days" ]
		if date_bucket not in date_buckets:
			self.write( {"status": 0, "message": "Invalid Date Bucket"} )

		try:
			news_obj = coll.find({"alert_id":alert_id})[0]
			temp = {}
			temp["newsdata"] = news_obj["data"][date_bucket + "_feed"][skip:skip+limit]
			temp["total"]    = len( news_obj["data"][date_bucket + "_feed"] )
			if news_obj["data"][date_bucket + "_feed"][skip+limit:skip+limit+limit] != []:
				temp["next"] = "http://utilities.paralleldots.com/news/feed?alert_id=%d&date_bucket=%s&skip=%d&limit=%d"%(alert_id,date_bucket,skip+limit,limit)
			temp["total"]    = len( news_obj["data"][date_bucket + "_feed"] )
			self.write( temp )

		except IndexError:
			try:
				uhex = hashlib.sha224( str( alert_id ) + date_bucket ).hexdigest()
				redisresp = sorted( json.loads( red.get( uhex ) ), key=lambda k:k["timestamp"], reverse=True )
				if limit > 20:
					limit = 20
				newsresp = {}
				newsresp["newsdata"] = redisresp[skip:][:limit]
				newsresp["total"] = len( redisresp )
				skip = skip+limit
				if skip < int(newsresp["total"]):
					newsresp["next"] = "http://utilities.paralleldots.com/news/feed?alert_id=%d&date_bucket=%s&skip=%d&limit=%d"%(alert_id,date_bucket,skip+limit,limit)
				self.write(json.dumps(newsresp))
			except:
				self.write( {"status": 0, "message": "Invalid alert_id"} )

		except Exception as e:
			print(e)
			self.write( {"status": 0, "message": "Oops Something Went Wrong"} )

class NERHandler(tornado.web.RequestHandler):
	def post(self):
		def process( string_data ):
			if string_data == "":
				return []
			WORD = []
			for word in string_data.split(","):
				if word[-1] == "\"" and word[0] == "\"":
					WORD.append(word)
				else:
					WORD = WORD + word.split(" ")
			return filter( lambda k: k != '', WORD )		
		self.set_header('Access-Control-Allow-Origin', '*')
		self.set_header('Access-Control-Allow-Credentials', 'true')
		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		uid   = self.get_argument("uid", None)
		required   = process(self.get_argument("required", ""))#[ x for x in self.get_argument("required", "").encode('utf-8').split(",") ]
		optional   = process(self.get_argument("optional", ""))#[ x for x in self.get_argument("optional", "").encode('utf-8').split(",") ]
		excluded   = process(self.get_argument("excluded", ""))#[ x for x in self.get_argument("excluded", "").encode('utf-8').split(",") ]
		publishers = [ x for x in self.get_argument("publishers", "").encode('utf-8').split(",") ]

		print required, optional, excluded, publishers

		timedeltas = { "12_hours": datetime.timedelta(hours=12), "24_hours": datetime.timedelta(days= 1), "2_days": datetime.timedelta(days=2), "7_days": datetime.timedelta(days=7) }
		date_bucket  = self.get_argument("date_bucket", None)
		if date_bucket not in timedeltas.keys():
			self.write( {"status": 0, "message": "Invalid Date Bucket"} )
		end_date   = datetime.datetime.utcnow().replace(minute=0, second=0)
		start_date = datetime.datetime.utcnow().replace(minute=0, second=0) - timedeltas[date_bucket]
		
		#try:
		n = pdnews.News()
		obj = n.complex_fetch( start_date, end_date, required, optional, excluded, publishers )["newsdata"]
		
		conn = database.connect(host=HOST,user=USER, password=PASSWORD, database=DATABASE)
		favlinks = []
		archivelinks = []
		favourites_data = conn.query('SELECT * FROM favourites WHERE user_id=%s',uid)
		favlinks = [json.loads(fav['data'])['link'] for fav in favourites_data]
		favdict = {}
		for fav in favourites_data:
			fd = json.loads(fav['data'])
			favdict[fd['link']]=fav['id']
		archive_data = conn.query('SELECT * FROM archive WHERE user_id=%s',uid)
		conn.close()

		response = []
		for item in obj:
			modified = item
			#if modified["link"] in favlinks:
			if modified["link"] in favlinks:
				modified["favorite"] = 1
				modified["fid"] = favdict[modified["link"]]
			else:
				modified["favorite"] = 0

			if modified["link"] in archivelinks:
				modified["archive"] = 1
			else:
				modified["archive"] = 0

			response.append( modified )
		
		response = sorted( response, key=lambda k:k["timestamp"], reverse=True)


		for item in response:
			item.pop("ner")
			item["timestamp"] = str( item["timestamp"] )
		final = { "feeds": { "newsdata": response }, "total": len( response ), "status": 1 }
		self.write( json.dumps( final ) )


def make_app():
	return tornado.web.Application([
		(r"/news/object", MainHandler),
		(r"/news/feed", FeedHandler),
		(r"/news/cloud",NERHandler),
	])

if __name__ == "__main__":
	app = make_app()
	app.listen(int(sys.argv[1]))
	tornado.ioloop.IOLoop.current().start()
