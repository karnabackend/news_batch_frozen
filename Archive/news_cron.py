from memsql.common import database
import datetime
import pymongo
import pdnews

# Loading alerts 
HOST = "memsql.paralleldots.com"
USER = "ankit"
PASSWORD = "mongodude123"
DATABASE = "socialmedia"
conn = database.connect(host=HOST,user=USER, password=PASSWORD, database=DATABASE)
memsql_objects = conn.query('SELECT * FROM myalerts')
conn.close()

coll = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["client_data"]

def process( string_data ):
	if string_data == "":
		return []
	WORD = []
	for word in string_data.split(","):
		if word[-1] == "\"" and word[0] == "\"":
			WORD.append(word)
		else:
			WORD = WORD + word.split(" ")
	return filter( lambda k: k != '', WORD )

#def remove_dots(data):
#    for key in data.keys():
#        if type(data[key]) is dict: data[key] = remove_dots(data[key])
#        if '.' in key:
#            data[key.replace('.', '\uff0E')] = data[key]
#            del data[key]
#    return data

valid_keys = pdnews.publishers.empty_get().keys()

for item in memsql_objects:
	uid         = item["uid"]
	required    = process( item["keyword"] )
	optional    = process( item["optional_kw"] )
	#excluded    = process( item["excluded_kw"] )
	excluded    = []
	start_dates = [ datetime.datetime.utcnow() - datetime.timedelta(hours=x) for x in [12, 24, 48, 168] ]
	end_date    = datetime.datetime.utcnow()
	publishers  = [ x for x in item["publisher"].split(",") ]
	temp = []
	for publisher in publishers:
		if publisher.lower() not in valid_keys:
			continue
		else:
			temp.append(publisher)
	publishers = temp
	alert_id = item["id"]

	obj = {}
	output = []
	for start_date in start_dates:
		n = pdnews.News()
		print start_date, end_date, required, optional, excluded
		n.complex_fetch( start_date, end_date, required, optional, excluded, publishers )
		n.customize_with_uid(uid)
		output.append( n.complex_process_for_cron() )

	# Building Base Object
	obj["alert_id"] = item["id"]
	obj["uid"] = uid
	obj["required"] = required
	obj["optional"] = optional
	obj["excluded"] = excluded

	for publisher in publishers:
		publisher = publisher.replace('.', '\uff0E')
	obj["publishers"] = publishers
	obj["data"] = {}
	if output[0] == {'message': 'No data found !', 'status': 0}:
		obj["data"]["12_hours"] = {'message': 'No data found !', 'status': 0}
		obj["data"]["12_hours_feed"] = {'message': 'No data found !', 'status': 0}
	else:
		obj["data"]["12_hours"] = output[0]
		obj["data"]["12_hours_feed"] = output[0].pop("cached")
		
	if output[1] == {'message': 'No data found !', 'status': 0}:
		obj["data"]["24_hours"] = {'message': 'No data found !', 'status': 0}
		obj["data"]["24_hours_feed"] = {'message': 'No data found !', 'status': 0}
	else:
		obj["data"]["24_hours"] = output[1]
		obj["data"]["24_hours_feed"] = output[1].pop("cached")
		
	if output[2] == {'message': 'No data found !', 'status': 0}:
		obj["data"]["2_days"] = {'message': 'No data found !', 'status': 0}
		obj["data"]["2_days_feed"] = {'message': 'No data found !', 'status': 0}
	else:
		obj["data"]["2_days"] = output[2]
		obj["data"]["2_days_feed"] = output[2].pop("cached")
		
	if output[3] == {'message': 'No data found !', 'status': 0}:
		obj["data"]["7_days"] = {'message': 'No data found !', 'status': 0}
		obj["data"]["7_days_feed"] = {'message': 'No data found !', 'status': 0}
	else:
		obj["data"]["7_days"] = output[3]
		obj["data"]["7_days_feed"] = output[3].pop("cached")

	# Checking for Old Objects
	try:
		old_item = coll.find({"alert_id":alert_id})[0]
		keys = ["12_hours_feed", "24_hours_feed", "2_days_feed", "7_days_feed"]
		keys = ["12_hours", "24_hours", "2_days", "7_days"]
		old_values = {}
		count = 0
		for key in keys:
			if old_item["data"][key] == {"status": 0, "message": "No data found !"}:
				old_values[key] = {"popularity_ratio": 0.1, "mention_count": 0.1, "avg_sentiment": "neutral", "scan_count": 0.1}
			else:
				root = old_item["data"][key]["topstats"]
				if root["popularity_ratio"] == 0:
					root["popularity_ratio"] = 0.1
				if root["mention_count"] == 0:
					root["mention_count"] = 0.1
				if root["scan_count"] == 0:
					root["scan_count"] = 0.1
				old_values[key] = {"popularity_ratio": root["popularity_ratio"], "mention_count": root["mention_count"], "avg_sentiment": root["avg_sentiment"], "scan_count": root["scan_count"]}

		for key in keys:
			if obj["data"][key] != {'message': 'No data found !', 'status': 0}:
				obj["data"][key]["topstats"]["diff"] = {}
				obj["data"][key]["topstats"]["diff"]["popularity_ratio"] = round( 100 * ( obj["data"][key]["topstats"]["popularity_ratio"] - old_values[key]["popularity_ratio"] ) / old_values[key]["popularity_ratio"], 2 )
				obj["data"][key]["topstats"]["diff"]["mention_count"]    = round( 100 * ( obj["data"][key]["topstats"]["mention_count"] - old_values[key]["mention_count"] ) / old_values[key]["mention_count"], 2 )
				obj["data"][key]["topstats"]["diff"]["scan_count"]       = round( 100 * ( obj["data"][key]["topstats"]["scan_count"] - old_values[key]["scan_count"] ) / old_values[key]["scan_count"], 2 )
				obj["data"][key]["topstats"]["diff"]["avg_sentiment"]    = {"old_sentiment": old_values[key]["avg_sentiment"], "news_sentiment": obj["data"][key]["topstats"]["avg_sentiment"]}

		coll.update({"_id": old_item["_id"]}, {"$set": {"data": obj["data"] }}, check_keys=False )



	except IndexError:
		# Building a New Object if no pre-exiting object
		coll.insert( obj, check_keys=False )