import pymongo

coll_alpha = pymongo.MongoClient("mongodb://104.155.210.134/client_text")["client_text"]["news_data"]
coll_omega = pymongo.MongoClient("mongodb://104.155.210.134/news2")["news2"]["data_stream_database"]

data = coll_omega.find({}).batch_size(500)

for item in data:
	if "taxonomy" not in item:
		tax = coll_alpha.find_one( { "link" : item["link"] } )
		if "taxonomy" not in tax:
			continue
		coll_omega.update({"_id": item["_id"]}, {"$set": {"taxonomy": tax["taxonomy"] }}, check_keys=False )