#!/usr/bin/python
from __future__ import print_function
import social_media_score
import report_process
import traceback
import operator
import datetime
import settings
import requests
import pymongo
import pdnews
import json
import csv
import sys
import os

FILE_ROOT = settings.FILE_ROOT

# Mongo DB Connection Details
MONGO_DB_SERVER_URL         = os.environ["MONGOURL"]
DB_NAME                     = "reports"

COLLECTION_NAME_final       = "news"
COLLECTION_NAME_data_stream = "news_data_stream"

coll_data_stream            = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_NAME_data_stream ]

# Function to break the required query into it"s component parts
def process_required( string_data ):
	if string_data in [ "", None, [] ]:
		return []
	WORDS = []
	for word in string_data.split( "," ):
		word = word.strip()
		WORDS.append( word )
	return filter( lambda k: k not in ["", [""], []], WORDS )

def long_range_csv( required, excluded, days, slug_id ):
	optional    = process_required( required )
	excluded    = process_required( excluded )

	start_date  = datetime.datetime.utcnow() - datetime.timedelta( days=days )
	end_date    = datetime.datetime.utcnow()

	# Runs each search terms for each query
	print( "%s - %s ### %s ### %s - "%( start_date, end_date, optional, excluded ), end="" )

	n    = pdnews.News()
	temp = n.fetch_without_publishers_without_sentimentv2( start_date, end_date, optional, excluded )

	data_stream = []

	print( "Count: %d"%( len( temp["newsdata"] ) ) )

	for temp_item in temp["newsdata"]:
		# Annotating Data
		temp_item["slug_id"] = slug_id

		if "ner" not in temp_item:
			try:
				temp_item["ner"] = requests.post( "http://annotate.apis.paralleldots.com/ner", data=json.dumps( { "text": temp_item["text"] } ) ).json()["entities"]
			except:
				print( traceback.format_exc() )
				temp_item["ner"] = []

		if "ner_heading" not in temp_item:
			try:
				temp_item["ner_heading"] = requests.post( "http://annotate.apis.paralleldots.com/ner", data=json.dumps( { "text": temp_item["Highlight"] } ) ).json()["entities"]
			except:
				print( traceback.format_exc() )
				temp_item["ner_heading"] = []

		if "sentimentv2" not in temp_item:
			try:		
				temp_item["sentimentv2"] = requests.get( "http://annotate.apis.paralleldots.com/sentiment?sentence1=" + temp_item["Highlight"] ).json()
			except:
				print( traceback.format_exc() )
				temp_item["sentimentv2"] = 0.5

		data_stream.append( temp_item )
		temp_item.pop( "_id", None )
		coll_data_stream.insert( temp_item, check_keys=False )
		temp_item.pop( "_id", None )
	
	print( "Dump Completed for slug_id - %s"%slug_id )

	# CSV File Writing Details
	headers       = [ "Timestamp", "Publisher", "Publisher Link", "Link", "Headline", "Description", "Sentiment Score", "Main Entity 1", "Main Entity 2", "Main Entity 3", "Main Entity 4", "Main Entity 5", "Heading Entity 1", "Heading Entity 2", "Heading Entity 3", "Facebook Score", "Linked-in Score" ]
	csv_file_name = "%sNews_CSV-_slug_id-%s_start_-_%s_end_-_%s.csv"%( FILE_ROOT, slug_id, str( start_date ), str( end_date ) )
	csv_file      = open( csv_file_name, "w" )
	csv_writer    = csv.writer( csv_file, quoting=csv.QUOTE_ALL, skipinitialspace=True )

	csv_writer.writerow( headers )

	# Populating CSV Rows
	for item in data_stream:
		# Processing and Setting Default Values for NERs and Heading_NERs
		if item.get( "ner", None ) == None:
			sorted_ners         = [ [None], [None], [None], [None], [None] ]
		elif type( item["ner"] ) in [ unicode, str ]:
			item["ner"]         = json.loads( item["ner"] )
			sorted_ners         = sorted( item["ner"], key=operator.itemgetter(1), reverse=True )
		else:
			sorted_ners         = sorted( item["ner"], key=operator.itemgetter(1), reverse=True )

		if item.get( "ner_heading", None ) == None:
			sorted_heading_ners = [ [None], [None], [None] ]
		else:
			sorted_heading_ners = sorted( item["ner_heading"], key=operator.itemgetter(1), reverse=True )
		
		sorted_ners         = sorted_ners         + [ [None] for i in range( 0, 5-len( sorted_ners ) ) ]
		sorted_heading_ners = sorted_heading_ners + [ [None] for i in range( 0, 3-len( sorted_heading_ners ) ) ]

		row_timestamp       = item["timestamp"].isoformat()
		row_publisher_name  = item.get( "publisher_name", None )
		row_publisher_link  = item.get( "client"        , None )
		row_link            = item["link"]
		row_headline        = item.get( "heading"    , ""   ).encode("utf8")
		row_description     = item.get( "description", ""   ).encode("utf8")
		row_sentiment_score = item.get( "sentimentv2", 0.5  )
		#row_sentiment_label = get_label( item.get( "sentimentv2", 0.5  ) )
		row_ner_1           = sorted_ners[0][0]
		row_ner_2           = sorted_ners[1][0]
		row_ner_3           = sorted_ners[2][0]
		row_ner_4           = sorted_ners[3][0]
		row_ner_5           = sorted_ners[4][0]
		row_heading_ner_1   = sorted_heading_ners[0][0]
		row_heading_ner_2   = sorted_heading_ners[1][0]
		row_heading_ner_3   = sorted_heading_ners[2][0]
		row_facebook_score  = item.get( "social_media_score", { "linkedin_score": 0, "fb_score": 0 } ).get( "fb_score"    , 0 )
		row_linkedin_score  = item.get( "social_media_score", { "linkedin_score": 0, "fb_score": 0 } ).get( "linked_score", 0 )

		row                 = [ row_timestamp, row_publisher_name, row_publisher_link, row_link, row_headline, row_description, row_sentiment_score, row_ner_1, row_ner_2, row_ner_3, row_ner_4, row_ner_5, row_heading_ner_1, row_heading_ner_2, row_heading_ner_3, row_facebook_score, row_linkedin_score ]

		csv_writer.writerow( row )

	csv_file.close()

	return json.dumps( { "status": 1, "message": "CSV File Successfully Created !", "file_name": csv_file_name } )

if __name__ == "__main__":
	print( "Data Entry Sequence - required( str ), excluded( str ), start_date( str ), end_date( str )" )
	required = sys.argv[1]
	excluded = sys.argv[2]
	days     = int( sys.argv[3] )
	slug_id  = sys.argv[4]
	long_range_csv( required, excluded, days, slug_id )